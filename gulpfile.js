var elixir = require('laravel-elixir')

var plugins = [
	"vendor/pixi.js/bin/pixi.js",
	"vendor/pixi-particles/dist/pixi-particles.js",
	"node_modules/pixi-tween/build/pixi-tween.js",
	"node_modules/pixi-audio/build/pixi-audio.js"
];

// Override the default config
elixir.config.publicPath = "build";
elixir.config.assetsPath = "src/assets";
elixir.config.appPath = "src/app";

elixir(function(mix) {
    mix.sass("style.scss")
    mix.copy("src/**/*.html", "build")
    mix.combine(plugins, 'build/js/plugins.js')
    mix.webpack('app.js')
    mix.copy('src/assets/images', 'build/images')
    mix.copy('src/assets/media', 'build/media')
});
