/**
 * Entry file to the application. call necessery functionality on window load
 */
import Loader from 			"../../../src/assets/js/components/loader.js";
import CONFIG from 			"../../../src/assets/js/components/config.js";
import Levels from 			"../../../src/assets/js/components/levels.js";
import sound from 			"../../../src/assets/js/components/sound.js";
import { Stringify } from 	"../../../src/assets/js/components/utils.js";
import Device from 			"../../../src/assets/js/components/device.js";

class Stage {
	constructor(o) {
		this.renderer = new PIXI.autoDetectRenderer(o.baseWidth, o.baseHeight);
        this.renderer.autoResize = true;
		o.container.appendChild(this.renderer.view);
		this.container = new PIXI.Container();

		let ratio = o.baseWidth / o.baseHeight;

        window.addEventListener("resize", () => {
            let containerWidth = document.getElementById('container').clientWidth;
            let containerHeight = document.getElementById('container').clientHeight;
            this.renderer.resize(containerWidth, containerHeight);
            o.baseWidth = window.app_setting.baseWidth = document.getElementById('container').clientWidth;
            o.baseHeight = window.app_setting.baseHeight = document.getElementById('container').clientHeight;
        }, false);

        return this;
	}
	//window.requestAnimationFrame(updateview);
	updateview() {
		//window.requestAnimationFrame( updateview );
		this.renderer.render(this.container);
	}
}

function initLoad() {
    if(typeof device != 'undefined' && device.platform == 'Android') {
        //CONFIG.baseImageUrl = 'file:///android_asset/www/' + CONFIG.baseImageUrl;
    }
	CONFIG.baseHeight = document.getElementById('container').clientHeight;
    CONFIG.baseWidth = document.getElementById('container').clientWidth;
	CONFIG.container = document.getElementById('container');
    //console.log(document.getElementById('container').clientHeight, document.getElementById('container').clientWidth);

	var assetLoader = new PIXI.loaders.Loader();
	var loader = new Loader('loading');
	loader.show();
	/*loader.add('bg', 'images/49.jpg');
	loader.add('level1', 'images/2.png');*/
	CONFIG.assets.map((item) => {
		// Adding try catch to avoid error
		try{
			assetLoader.add(CONFIG.baseImageUrl + item);
		}
		catch(e) {
			console.log(e)
		}
	});



	// Load level selection audio
	let d = new Device();
    if(typeof cordova == 'undefined') {
        /**
         * Load Media files
         */
         CONFIG.introMedia.map((md) => {
             // Adding try catch to avoid error
             try{
                assetLoader.add(CONFIG.baseMediaUrl + md);
             }
             catch(e) {
                console.log(e)
             }
         });
        // Adding try catch to avoid error
         try {
             // For mobile
             assetLoader.add(CONFIG.baseMediaUrl + CONFIG.welcomeAudioMobile);
             // For Desktop
             assetLoader.add(CONFIG.baseMediaUrl + CONFIG.welcomeAudioDesktop);
             // For mobile
             assetLoader.add(CONFIG.baseMediaUrl + CONFIG.welcomeAllUnlockedMobile);
             // For desktop
             assetLoader.add(CONFIG.baseMediaUrl + CONFIG.welcomeAllUnlockedDesktop);
             // For mobile
             assetLoader.add(CONFIG.baseMediaUrl + CONFIG.waitngAudioMobile);
             // For Desktop
             assetLoader.add(CONFIG.baseMediaUrl + CONFIG.waitngAudioDesktop);
             assetLoader.add(CONFIG.baseMediaUrl + CONFIG.notEnoughCoin);

             assetLoader.add(CONFIG.baseMediaUrl + CONFIG.differentChoiceMobile);
             assetLoader.add(CONFIG.baseMediaUrl + CONFIG.differentChoiceDesktop);

             assetLoader.add(CONFIG.baseMediaUrl + CONFIG.click_sound);
         }
         catch(e) {
            console.log(e);
         }
        CONFIG.levels.levelAudio.map((md) => {
            // Adding try catch to avoid error
            try{
                if( d.isMobile() ) {
                    assetLoader.add(CONFIG.baseMediaUrl + md.mobile);
                } else {
                    assetLoader.add(CONFIG.baseMediaUrl + md.desktop);
                }
            }
            catch(e) {
                console.log(e);
            }
        });
    }


    let afterLoadAssets = function() {
        window.app_setting = CONFIG;
        /*if(!localStorage.getItem('elmo')) {
         localStorage.setItem('elmo', new Stringify(window.app_setting).toString());
         }*/
        /*if(JSON.stringify(window.app_setting) != localStorage.getItem('elmo')) {
         localStorage.setItem('elmo', JSON.stringify(window.app_setting));
         }*/
        let stage = new Stage(CONFIG);

        let levels = new Levels(stage.container, window.app_setting, stage.renderer);
        levels.addBackground();
        levels.addLevels();
        loader.hide();
        stage.updateview();
        let playInitialAudio = () => {
            //window.removeEventListener('touchstart', playInitialAudio, false);
            sound.play(CONFIG.baseMediaUrl + CONFIG.introMedia[0], () => {
                var d = new Device();
                var allUnlocked = true;
                for(let i = 0;i < CONFIG.levels.group.length;i++) {
                    if(CONFIG.levels.group[i].locked == true) {
                        allUnlocked = false;
                    }
                }
                if(allUnlocked) {
                    // Check if mobile or desktop
                    if(d.isMobile()) {
                        sound.play(CONFIG.baseMediaUrl + CONFIG.welcomeAllUnlockedMobile);
                    } else {
                        sound.play(CONFIG.baseMediaUrl + CONFIG.welcomeAllUnlockedDesktop);
                    }
                } else {
                    // Check if mobile or desktop
                    if(d.isMobile()) {
                        sound.play(CONFIG.baseMediaUrl + CONFIG.welcomeAudioMobile);
                    } else {
                        sound.play(CONFIG.baseMediaUrl + CONFIG.welcomeAudioDesktop);
                    }
                }

                window.delaySound = setTimeout(() => {
                    var d = new Device();
                    if( d.isMobile() ) {
                        sound.play(CONFIG.baseMediaUrl + CONFIG.waitngAudioMobile);
                    } else {
                        sound.play(CONFIG.baseMediaUrl + CONFIG.waitngAudioDesktop);
                    }
                    //window.clearTimeout(delaySound);
                }, 8000);

            });
        }
        let userAgent = window.navigator.userAgent;
        if(userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
            /**
             * THis code will only work on iOS devices
             * On click it will initiate the play audio for IOS devices
             * @type {Element}
             */
            var playButton = document.getElementById('play-button');
            document.getElementById('play-button-container').style.display = 'block';
            playButton.addEventListener('click', function () {
                playInitialAudio();
                document.getElementById('play-button-container').style.display = 'none';
            });
            //window.addEventListener('touchstart', playInitialAudio , false);
        } else {
            playInitialAudio();
        }
        // Intro Audio play
    }

    assetLoader.once('complete',() => {
        afterLoadAssets()
    });
    assetLoader.once('error', () => {
        console.log('error');
    });
    assetLoader.load();


}

if (typeof cordova != 'undefined') {
	//console.log("dskldsds skdls d skdls");
	document.addEventListener("deviceready", function() {
        initLoad();
	}, false);
    document.addEventListener('pause', function () {
        sound.pause();
    });
    document.addEventListener('resume', function () {
        sound.resume();
    });
} else {
	window.addEventListener('load', function() {
       initLoad();
	});

    window.addEventListener('unload', function(event) {
        sound.pause();
    });
    document.addEventListener("visibilitychange", function () {
        if (document.hidden) {
            sound.pause();
        } else  {
            sound.resume();
        }
    }, false);
}
