/**
 * { Animation class }
 */
class Animate {
    /**
     * [constructor Defining all the variables]
     * @param  {[Object]} config [All param related to animation]
     * @return {[Object]}        [Current Context]
     */
    constructor(config) {
        this.config = config;
        this.animate_container = new PIXI.Container();
        this.animate_container.x = 0;
        this.animate_container.y = 0;
        this.animate_container.height = this.config.containerHeight;
        this.animate_container.width = this.config.containerWidth;
        this.animation_frame = null;
        this.elmo = null;
        this.bg = null;
    }
    /**
     * Animation for the last stage
     */
    climaxAnimate() {
        //Animation frames
        let frames = [];
        // Adding all frames to frames array
        for (let key in this.config.imgArr) {
          frames.push(PIXI.Texture.fromImage(key));
        }
        // creating movie clip from the frames
        this.elmo = new PIXI.extras.MovieClip(frames);
        // Setting property for the frames
        this.elmo.height = this.config.height;
        this.elmo.width = this.elmo._height * this.config.ratio;
        this.elmo.x = this.config.x;
        this.elmo.y = this.config.y;
        this.elmo.animationSpeed = 0.4;
        //start animation
        this.elmo.play();
        // Add animation frames to container
        this.animate_container.addChild(this.elmo);
        // Update the animation
        this.doAnimate();

    }
    /**
     * Move the animation
     */
    bgMoveAnimation() {
        // background frames
        let bg_frames = [];
        /*for(let k = 0;k < this.config.background.length;k += 1) {
            bg_frames.push(PIXI.Texture.fromImage(this.config.baseUrl + this.config.background[k]));
        }*/
        // Texture for the background image
        let texture = new PIXI.Texture.fromImage(this.config.baseUrl + this.config.background[0]);
        //console.log(texture);
        //Updating the baseTexture property for the backgroud image
        texture.baseTexture.height = this.config.containerHeight;
        //texture.baseTexture.width = texture.baseTexture.width * (this.config.containerHeight / this.config.containerWidth);
        // Creating a long sprite from the background image
        this.bg = new PIXI.extras.TilingSprite(texture);
        this.bg.position.x = 0;
        this.bg.position.y = 0;
        this.bg.height = this.config.containerHeight;
        this.bg.width = this.bg._height / (this.bg.texture.height / this.bg.texture.width);
        this.bg.tilePosition.x = 0;
        this.bg.tilePosition.y = 0;
        this.animate_container.addChild(this.bg);
        this.doAnimate(() => {
            if(this.bg) {
                this.bg.tilePosition.x -= 3;
                return true;
            } else {
                return;
            }

        });
    }
    /**
     * [doAnimate running the animation frames]
     * @param  {Function} callback [after animation complete]
     * @return {[Object]}            [description]
     */
    doAnimate(callback){
        this.config.renderer.render(this.config.container);
        // Animation frames
        let requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        // Start animation frames
        this.animation_frame = requestAnimationFrame(() => {
            if(!this.animation_frame) {
                return;
            }
            // If callback the call function
            if(callback) {
                callback();
                this.doAnimate(callback);
            } else {
                this.doAnimate();
            }

        });
    }
    /**
     * [cancelAnimate stop animation frames]
     * @return {[Object]} [animation frame]
     */
    cancelAnimate() {
        if(window.cancelAnimationFrame) {
            window.cancelAnimationFrame(this.animation_frame);
        }
        this.animation_frame = null;
    }
    /**
     * [destroy Deallocate all from memory]
     * @return {[type]} [description]
     */
    destroy() {
        this.config = null;
        //this.animate_container.destroy();
        this.animate_container = null;
        this.animation_frame = null;
        this.elmo = null;
        this.bg = null;
    }
}
/**
 * Other animation class with static methods
 */
class Anim {
    static moveAnimation(obj, to , time, callback) {
        let item = PIXI.tweenManager.createTween(obj);
        item.easing = PIXI.tween.Easing.inCubic();
        item.time = time;
        item.loop = false;
        //coinAnimate.path = path;
        //coinAnimate.pingPong = true;
        //
        item.to({
            position: {x: to.x, y: to.y },
        });
        item.on('end', () => {
            item.stop().clear();
            if(callback) {
                callback();
            }
        });
        item.start();
    }
    /**
     * [start start animation frames]
     * @param  {Function} callback [after animation complete]
     * @return {[Object]}            [void]
     */
    start(callback) {
        this.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        this.anim_frame = requestAnimationFrame(() => {
            if(callback) {
                callback();
                this.start(callback);
            }
        });
    }
    /**
     * [stop Stop animation]
     * @param  {Function} callback [after animation stop]
     * @return {[type]}            [description]
     */
    stop(callback) {
        if(window.cancelAnimationFrame) {
            window.cancelAnimationFrame(this.anim_frame);
            callback();
        }
    }
    /**
     * { Animate the coins on voice overs }
     */
    static animateCoins(from, to) {
        let isVisible = false;
        from.visible = false;
        to.visible = true;
        setTimeout(() => {
             let animInter = setInterval(() => {
                if(isVisible) {
                    from.visible = false;
                    to.visible = true;
                } else {
                    from.visible = true;
                    to.visible = false;
                }
                isVisible = !isVisible;
            }, 100);
            setTimeout(() => {
                from.visible = true;
                to.visible = false;
                window.clearInterval(animInter);
            }, 1100)
        }, 2500);


    }

    static resetCoinAnimation(children, url) {

    }
    /**
     * { animation for register box }
     *
     * @param      {<Object>}  coinContainer  The coin container
     * @param      {<Object>}  registerCount  The register count
     */
    static animateRegisterBox(coinContainer, registerCount) {
        //this.registerCount
        //console.log(this.coinContainer.scale.x);
        //Animating the coin container
        let count = 0;
        let coinNumberCount = 0;
        let tw = PIXI.tweenManager.createTween(coinContainer);
        tw.easing = PIXI.tween.Easing.outBounce();
        tw.time = 250;
        tw.loop = true;
        tw.pingPong = true;
        tw.to({
          scale: {x: coinContainer.scale.x * 1.05, y: coinContainer.scale.y * 1.05}
        });
        tw.on('end', () => {
            tw.stop().clear();
        });
        tw.on('pingpong', () => {
            count++;
            if(count == 5) {
                tw.stop().clear();
                coinContainer.scale.x = coinContainer.scale.x / 1.05;
                coinContainer.scale.y = coinContainer.scale.y / 1.05;
            }
        });
        tw.start();


        // Animating the coin count
        let countAnimate = PIXI.tweenManager.createTween(registerCount);
        countAnimate.easing = PIXI.tween.Easing.outBounce();
        countAnimate.time = 250;
        countAnimate.loop = true;
        countAnimate.pingPong = true;
        countAnimate.to({
            scale: {x: registerCount.scale.x * 1.1, y: registerCount.scale.y * 1.1}
        });
        countAnimate.on('end', () => {
            countAnimate.stop().clear();
        });
        countAnimate.on('pingpong', () => {
            coinNumberCount++;
            if(coinNumberCount == 5) {
                countAnimate.stop().clear();
                registerCount.scale.x = registerCount.scale.x / 1.1;
                registerCount.scale.y = registerCount.scale.y / 1.1;
            }
        });
        countAnimate.start();
    }

    static pathAnimation(options, container, to, time, count, callback) {
        for(let i = 0;i < count; i++) {
            setTimeout(() => {
                let coin = new PIXI.Sprite.fromImage(options.baseImageUrl + 'coin.png');
                if(window.innerWidth < 768) {
                    coin.height = options.baseHeight / 11.23478;
                } else {
                    coin.height = options.baseHeight / 13.23478;
                }
                coin.width = coin._height / (coin.texture.height / coin.texture.width);
                coin.x = options.baseWidth / 1.3368;
                coin.y = options.baseHeight / 1.48;
                coin.scale.x = coin.scale.x * 2.5;
                coin.scale.y = coin.scale.y * 2.5;

                container.addChild(coin);

                /*var path = new PIXI.tween.TweenPath();
                 path.quadraticCurveTo(to.x, to.y);*/

                let coinAnimate = PIXI.tweenManager.createTween(coin);
                coinAnimate.easing = PIXI.tween.Easing.inCubic();
                coinAnimate.time = time;
                coinAnimate.loop = false;
                //coinAnimate.path = path;
                //coinAnimate.pingPong = true;
                //
                coinAnimate.to({
                    position: {x: to.x, y: to.y },
                    scale: { x : (coin.scale.x / 2.5), y : (coin.scale.y / 2.5) }
                });
                coinAnimate.on('end', () => {
                    coin.visible = false;
                    coinAnimate.stop().clear();
                    container.removeChild(coin);
                    if(i == (count - 1)) {
                        //console.log(i);
                        if(callback) {
                            callback();
                        }
                    }
                });
                coinAnimate.start();
            }, 500 * i);

        }

    }

    static coinClickAnimation(options, container, from, to, time, callback) {
        let coin = new PIXI.Sprite.fromImage(options.baseImageUrl + 'coin.png');
        if(window.innerWidth < 768) {
            coin.height = options.baseHeight / 11.23478;
        } else {
            coin.height = options.baseHeight / 13.23478;
        }
        coin.width = coin._height / (coin.texture.height / coin.texture.width);
        coin.x = from.x;
        coin.y = from.y;
        coin.scale.x = coin.scale.x * 2.5;
        coin.scale.y = coin.scale.y * 2.5;

        container.addChild(coin);

        /*var path = new PIXI.tween.TweenPath();
        path.quadraticCurveTo(to.x, to.y);*/

        let coinAnimate = PIXI.tweenManager.createTween(coin);
        coinAnimate.easing = PIXI.tween.Easing.inCubic();
        coinAnimate.time = time;
        coinAnimate.loop = false;
        //coinAnimate.path = path;
        //coinAnimate.pingPong = true;
        //
        coinAnimate.to({
            position: {x: to.x, y: to.y },
            scale: { x : (coin.scale.x / 2.5), y : (coin.scale.y / 2.5) }
        });
        coinAnimate.on('end', () => {
            coin.visible = false;
            coinAnimate.stop().clear();
            container.removeChild(coin);
            if(callback) {
                callback();
            }
        });
        coinAnimate.start();
    }

    static moveCoins(coins, start, callback) {
        let count = 0;
        let moveCoinCount = start;
        for(let m = start; m >= 0; m -= 1) {
            if(coins[m].visible == true) {
                var tween = PIXI.tweenManager.createTween(coins[m]);
                tween.time = 200;
                tween.easing = PIXI.tween.Easing.inCubic();
                tween.loop = false;
                //tween.pingPong = true;
                tween.to({
                    position: {
                        x: coins[m].position.x + coins[m].parent._height + 10
                    }
                });
                tween.start();
                tween.on('end', () => {
                    coins[m].position.x = coins[m].position.x - (coins[m].parent._height + 10);
                    tween.stop().clear();
                    count++;
                    //console.log(moveCoinCount , count);
                    if(moveCoinCount == count) {
                        //console.log("ksksksksksks");
                        callback ? callback() : '';
                    }
                })
            } else {
                moveCoinCount--;
                if(moveCoinCount == count) {
                    //console.log("ksksksksksks");
                    callback ? callback() : '';
                }
            }
        }
    }

    static reIlluminateOverlay(overlay) {
        overlay.visible = true;
        setTimeout(() => {
            overlay.visible = false;
        }, 100);
        setTimeout(() => {
            overlay.visible = true;
        }, 200);
        setTimeout(() => {
            overlay.visible = false;
        }, 300);
        setTimeout(() => {
            overlay.visible = true;
        }, 400)
        setTimeout(() => {
            overlay.visible = false;
        }, 500)
    }
}

export { Animate , Anim };
