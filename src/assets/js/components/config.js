export default {
	"baseImageUrl": "images/",
	"baseMediaUrl": "media/",
	"baseHeight": "",
	"baseWidth": "",
	"container": "",
	"bgColor": 0xFFFFFF,
	"assets": [
		'Background_Bike_Shop_Shelves.png', 'Background_Outside.png', 'Background_Outside_Loop.png', 'Button_Home.png', 'Cash_Register.png', 'Cash_Register_Number_0.png', 'Cash_Register_Number_1.png', 'Cash_Register_Number_2.png', 'Cash_Register_Number_3.png', 'Cash_Register_Number_4.png', 'Cash_Register_Number_5.png', 'Cash_Register_Number_6.png', 'Cash_Register_Number_7.png', 'Cash_Register_Number_8.png', 'Cash_Register_Number_9.png', 'Cash_Register_Number_10.png', 'Cash_Register_Number_11.png', 'Choose_Cookie.png', 'Choose_Cookie_Locked.png', 'Choose_Elmo.png', 'Choose_Grover.png', 'Choose_Grover_Locked.png', 'coin.png', 'coin_jar.png', 'Shelf_Shadow_top.png', 'Shelf_Shadow_top_middle.png', 'Shelf_Shadow_top_middle_bottom.png', 'Title_Background.png', 'thought_1.png', 'thought_2.png', 'thought_3.png', 'thought_4.png', 'thought_5.png', 'thought_6.png', 'thought_7.png', 'thought_8.png', 'thought_9.png', 'coin_half.png', 'skip.png', 'coin-glow.png', 'coin_bg.png'
	],
	"homeIcon": "Button_Home.png",
	"introMedia" : [
		"BS_1.0_1.wav",
	],
	"coinContainer": "Cash_Register.png",
	"welcomeAudioMobile": "BS_1.1_15.wav",
	"welcomeAudioDesktop": "BS_1.1_16.wav",
    "welcomeAllUnlockedMobile" : "BS_1.1_9.wav",
    "welcomeAllUnlockedDesktop" : "BS_1.1_10.wav",
	"waitngAudioMobile" : "BS_1.1_13.wav",
	"waitngAudioDesktop" : "BS_1.1_14.wav",
	"notEnoughCoin" : "BS_4.5_117.wav",
	"differentChoiceMobile" : "BS_2.2_44.wav",
	"differentChoiceDesktop" : "BS_2.2_43.wav",
	"click_sound" : "click_sound.ogg",
    "nextArrow": "skip.png",
    "levels": {
		"group": [
			{
				"image": "Choose_Elmo.png",
				"name" : "elmo",
				"image_active": "Choose_Elmo.png",
				"audio_assets" : ['BS_2.1_15.wav', 'BS_2.1_19.wav', 'BS_2.2_20.wav', 'BS_2.2_21.wav', 'BS_2.2_22.wav', 'BS_2.2_23.wav', 'BS_2.2_24.wav', 'BS_2.2_25.wav', 'BS_2.2_30.wav', 'BS_2.2_31.wav', 'BS_2.2_35.wav', 'BS_2.2_36.wav', 'BS_5.0_120.wav', 'BS_5.0_121.wav', 'BS_5.0_122.wav', 'BS_5.0_123.wav', 'BS_5.0_124.wav', 'BS_5.0_125.wav', 'BS_5.0_126.wav', 'BS_5.0_127.wav', 'BS_5.0_128.wav', 'BS_5.0_129.wav', 'BS_2.2_37.wav', 'BS_2.2_38.wav', 'BS_5.0_130.wav', 'BS_5.0_131.wav', 'BS_5.0_132.wav', 'BS_5.0_133.wav', 'BS_5.0_134.wav', 'BS_5.0_135.wav', 'BS_5.0_136.wav', 'BS_5.0_137.wav', 'BS_5.0_138.wav', 'BS_5.0_139.wav', 'BS_2.2_41.wav', 'BS_2.2_42.wav', 'BS_2.3_56.wav', 'BS_2.3_57.wav', 'BS_2.3_58.wav', 'BS_2.3_59.wav', 'BS_2.4_62.wav', 'coin_return.mp3', 'BS_2.2_26.wav', 'BS_2.2_27.wav', 'BS_2.2_48.wav', 'BS_2.2_49.wav', 'BS_2.2_50.wav', 'BS_2.2_51.wav', 'BS_2.2_46.wav', 'BS_2.2_47.wav', 'elmo_payoff.mp3', "BS_2.2_54.wav", "BS_2.3_60.wav", "BS_2.3_61.wav", "BS_2.2_52.wav","BS_2.2_53.wav"
				],
				"image_assets" : [
					"elmo/Elmo_Bubble_Bicycle_Broken_01.png","elmo/Elmo_Bubble_Bicycle_Broken_02.png", "elmo/Elmo_Bubble_Bicycle_Broken_03.png", "elmo/Elmo_Bubble_Bicycle_Fixed.png", "elmo/Elmo_Basket_Flower_withtag.png", "elmo/Elmo_Basket_Star_withtag.png", "elmo/Elmo_Seat_Blue_withtag.png", "elmo/Elmo_Seat_Purple_withtag.png",
					"elmo/Elmo_Wheel_Rubber_withtag.png", "elmo/Elmo_Wheel_Wood_withtag.png", "elmo/elmo_looking.png", "elmo/Background_Bike_Shop_dark_Elmo.png", "elmo/Elmo_Bicycle_Blw.png","elmo/Elmo_Bicycle_Brw.png", "elmo/Elmo_Bicycle_Silo_Blw.png", "elmo/Elmo_Bicycle_Silo_Brw.png", "elmo/Brw_Elmo_Bicycle_Silo_Bs.png", "elmo/Blw_Elmo_Bicycle_Silo_Bs.png", "elmo/Elmo_Bicycle_Silo_Bs_Blw.png", "elmo/Elmo_Bicycle_Silo_Bs_Brw.png", "elmo/Brw_Elmo_Bicycle_Silo_Fb.png", "elmo/Blw_Elmo_Bicycle_Silo_Fb.png", "elmo/Elmo_Bicycle_Silo_Fb_Blw.png", "elmo/Elmo_Bicycle_Silo_Fb_Brw.png", "elmo/Blw_Elmo_Bicycle_Silo_Fb_Bs.png", "elmo/Brw_Elmo_Bicycle_Silo_Fb_Bs.png", "elmo/Elmo_Bicycle_Silo_Fb_Bs_Blw.png", "elmo/Elmo_Bicycle_Silo_Fb_Bs_Brw.png", "elmo/Brw_Elmo_Bicycle_Silo_Fb_Ps.png","elmo/Blw_Elmo_Bicycle_Silo_Fb_Ps.png", "elmo/Elmo_Bicycle_Silo_Fb_Ps_Blw.png", "elmo/Elmo_Bicycle_Silo_Fb_Ps_Brw.png", "elmo/Brw_Elmo_Bicycle_Silo_Ps.png", "elmo/Blw_Elmo_Bicycle_Silo_Ps.png", "elmo/Elmo_Bicycle_Silo_Ps_Blw.png", "elmo/Elmo_Bicycle_Silo_Ps_Brw.png", "elmo/Brw_Elmo_Bicycle_Silo_Sb.png", "elmo/Blw_Elmo_Bicycle_Silo_Sb.png", "elmo/Elmo_Bicycle_Silo_Sb_Blw.png", "elmo/Elmo_Bicycle_Silo_Sb_Brw.png", "elmo/Blw_Elmo_Bicycle_Silo_Sb_Bs.png", "elmo/Brw_Elmo_Bicycle_Silo_Sb_Bs.png", "elmo/Elmo_Bicycle_Silo_Sb_Bs_Blw.png", "elmo/Elmo_Bicycle_Silo_Sb_Bs_Brw.png", "elmo/Brw_Elmo_Bicycle_Silo_Sb_Ps.png", "elmo/Blw_Elmo_Bicycle_Silo_Sb_Ps.png", "elmo/Elmo_Bicycle_Silo_Sb_Ps_Blw.png", "elmo/Elmo_Bicycle_Silo_Sb_Ps_Brw.png", "elmo/Elmo_Basket_Star.png", "elmo/Elmo_Basket_Flower.png", "elmo/Elmo_Seat_Blue.png", "elmo/Elmo_Seat_Purple.png", "elmo/Elmo_Wheel_Rubber.png", "elmo/Elmo_Wheel_Wood.png", "elmo/Elmo_Basket_Flower_withtag_glow.png", "elmo/Elmo_Basket_Star_withtag_glow.png", "elmo/Elmo_Seat_Blue_withtag_glow.png", "elmo/Elmo_Seat_Purple_withtag_glow.png", "elmo/Elmo_Wheel_Rubber_withtag_glow.png", "elmo/Elmo_Wheel_Wood_withtag_glow.png"
				],
				"locked": false,
				"played" : false,
				"totalCoin": 10,
				"coinIcon": "coin.png",
				"coinIconGlow": "coin-glow.png",
				"registerCoin": "coin_half.png",
				"registerCount": "Cash_Register_Number_",
				"elmo": "elmo/elmo_looking.png",
				"climaxBg": [
					"Background_Outside_Loop.png",
					"48-2.jpg"
				],
				"introAudio": [
					"BS_2.1_15.wav",
					"BS_2.1_19.wav"
				],
				"totalProduct": {
					"itemBg": "41.png",
					"cyclebg": "elmo/Background_Bike_Shop_dark_Elmo.png",
					"cycle": "elmo/Elmo_Bicycle_Blw.png",
					"cycle_alt": "elmo/Elmo_Bicycle_Brw.png",
					"alias" : "Elmo_Bicycle_Silo",
					"boughtCompleteAudio" : "BS_2.2_41.wav",
					"fixAudio" : "BS_2.2_42.wav",
					"dragAudio" : "BS_2.3_56.wav",
                    "dragAudioDesktop" : "BS_2.3_57.wav",
					"delayAudio" : "BS_2.3_58.wav",
                    "delayAudioDesktop" : "BS_2.3_59.wav",
					"dragCompleteAudio" : "BS_2.4_62.wav"
				},
				"thoughtBubble": [
					"thought_1.png",
					"thought_2.png",
					"thought_3.png",
					"thought_4.png",
					"thought_5.png",
					"thought_6.png",
					"thought_7.png",
					"thought_8.png",
					"thought_9.png"
				],
				"intro":{
					"user": ["elmo/user_1.png","elmo/user_2.png"],
					"position": ["left","left", "left", "left", "left"],
					"thoughtContent": ["elmo/Elmo_Bubble_Bicycle_Broken_01.png","elmo/Elmo_Bubble_Bicycle_Broken_02.png", "elmo/Elmo_Bubble_Bicycle_Broken_03.png", "elmo/Elmo_Bubble_Bicycle_Fixed.png", "coin_jar.png"],
					"background" : "Background_Outside.png",
					"lastAudioTime": 8000,
					"coinAudio": "BS_2.2_20.wav",
					"register": "BS_2.2_21.wav",
					"tapMobile" : "BS_2.2_22.wav",
					"clickDesktop" : "BS_2.2_23.wav"
				},
				"game": {
					"bgImage": "Background_Bike_Shop_Shelves.png",
					"items": [
						[
							{
								"img": "elmo/Elmo_Seat_Blue_withtag.png",
								"img_glow": "elmo/Elmo_Seat_Blue_withtag_glow.png",
								"alias": "Bs",
								"price": 3,
								"priority" : 1,
								"head" : false,
								"bought_img" : "elmo/Elmo_Seat_Blue.png",
								"clickAudio" : "BS_2.2_25.wav",
								"chooseAudio" : "BS_2.2_47.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_2.2_46.wav",
                                "returnItemDesktop": "BS_2.2_47.wav",
                                "delayPlay": ["BS_2.2_46.wav", "BS_2.2_47.wav"]
							},
							{
								"img": "elmo/Elmo_Seat_Purple_withtag.png",
								"img_glow": "elmo/Elmo_Seat_Purple_withtag_glow.png",
								"alias": "Ps",
								"price": 3,
								"priority" : 1,
								"head" : false,
								"bought_img" : "elmo/Elmo_Seat_Purple.png",
								"clickAudio" : "BS_2.2_25.wav",
								"chooseAudio" : "BS_2.2_24.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_2.2_46.wav",
                                "returnItemDesktop": "BS_2.2_47.wav",
                                "delayPlay": ["BS_2.2_46.wav", "BS_2.2_47.wav"]
							}
						],
						[
							{
								"img": "elmo/Elmo_Basket_Flower_withtag.png",
								"img_glow": "elmo/Elmo_Basket_Flower_withtag_glow.png",
								"alias": "Fb",
								"price": 2,
								"priority" : 0,
								"head" : false,
								"bought_img" : "elmo/Elmo_Basket_Flower.png",
								"clickAudio" : "BS_2.2_31.wav",
								"chooseAudio" : "BS_2.2_30.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_2.2_48.wav",
                                "returnItemDesktop": "BS_2.2_49.wav",
                                "delayPlay": ["BS_2.2_48.wav", "BS_2.2_49.wav"]
							},
							{
								"img": "elmo/Elmo_Basket_Star_withtag.png",
								"img_glow": "elmo/Elmo_Basket_Star_withtag_glow.png",
								"alias": "Sb",
								"price": 2,
								"priority" : 0,
								"head" : false,
								"bought_img" : "elmo/Elmo_Basket_Star.png",
								"clickAudio" : "BS_2.2_31.wav",
								"chooseAudio" : "BS_2.2_30.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_2.2_48.wav",
                                "returnItemDesktop": "BS_2.2_49.wav",
                                "delayPlay": ["BS_2.2_48.wav", "BS_2.2_49.wav"]
							}
						],
						[
							{
								"img": "elmo/Elmo_Wheel_Rubber_withtag.png",
								"img_glow": "elmo/Elmo_Wheel_Rubber_withtag_glow.png",
								"alias": "Blw",
								"price": 4,
								"priority" : 2,
								"head" : false,
								"bought_img" : "elmo/Elmo_Wheel_Rubber.png",
								"clickAudio" : "BS_2.2_36.wav",
								"chooseAudio" : "BS_2.2_35.wav",
                                "payFor" : "BS_2.2_37.wav",
                                "payForDesktop" : "BS_2.2_38.wav",
                                "returnItem" : "BS_2.2_50.wav",
                                "returnItemDesktop": "BS_2.2_51.wav",
                                "delayPlay": ["BS_2.2_50.wav", "BS_2.2_51.wav"]
							},
							{
								"img": "elmo/Elmo_Wheel_Wood_withtag.png",
								"img_glow": "elmo/Elmo_Wheel_Wood_withtag_glow.png",
								"alias": "Brw",
								"price": 4,
								"priority" : 2,
								"head" : false,
								"bought_img" : "elmo/Elmo_Wheel_Wood.png",
								"clickAudio" : "BS_2.2_36.wav",
								"chooseAudio" : "BS_2.2_35.wav",
                                "payFor" : "BS_2.2_37.wav",
                                "payForDesktop" : "BS_2.2_38.wav",
                                "returnItem" : "BS_2.2_50.wav",
                                "returnItemDesktop": "BS_2.2_51.wav",
                                "delayPlay": ["BS_2.2_50.wav", "BS_2.2_51.wav"]
							}
						]
					],

				},
                "payoff": 'elmo_payoff.mp3',
                "fixDelay" : ["BS_2.3_60.wav", "BS_2.3_61.wav"],
                "payDelay" : ["BS_2.2_52.wav","BS_2.2_53.wav"],
                "finishAddCoin" : "BS_2.2_54.wav"
            },
			{
				"image": "Choose_Grover_Locked.png",
				"name" : "grover",
				"image_active": "Choose_Grover.png",
				"audio_assets" : [
					'BS_5.0_120.wav', 'BS_5.0_121.wav', 'BS_5.0_122.wav', 'BS_5.0_123.wav', 'BS_5.0_124.wav', 'BS_5.0_125.wav', 'BS_5.0_126.wav', 'BS_5.0_127.wav', 'BS_5.0_128.wav', 'BS_5.0_129.wav', 'BS_2.2_37.wav', 'BS_2.2_38.wav', 'BS_5.0_130.wav', 'BS_5.0_131.wav', 'BS_5.0_132.wav', 'BS_5.0_133.wav', 'BS_5.0_134.wav', 'BS_5.0_135.wav', 'BS_5.0_136.wav', 'BS_5.0_137.wav', 'BS_5.0_138.wav', 'BS_5.0_139.wav', "BS_3.1_66.wav", "BS_3.1_70.wav", "BS_3.2_71.wav", "BS_2.2_21.wav", "BS_2.2_22.wav", "BS_2.2_23.wav", "BS_4.2_99.wav", "BS_4.2_100.wav", "BS_2.2_36.wav", "BS_2.2_35.wav", "BS_3.2_75.wav", "BS_3.2_76.wav", "BS_3.2_87.wav", "BS_3.2_88.wav" ,"BS_3.3_84.wav", "BS_3.3_85.wav", "BS_3.3_82.wav", "BS_3.3_83.wav", "coin_return.mp3", "BS_3.3_88.wav", "BS_2.2_26.wav", "BS_2.2_27.wav", "BS_2.2_37.wav", "BS_2.2_38.wav", "BS_3.2_93.wav", "BS_3.2_92.wav", "BS_3.2_80.wav", "BS_3.2_81.wav", "BS_2.2_50.wav", "BS_2.2_51.wav", "grover_payoff.mp3", "BS_2.2_52.wav", "BS_2.2_53.wav", "BS_2.2_54.wav", "BS_3.3_86.wav", "BS_3.3_87.wav"
				],
				"image_assets" : [
					"grover/user_1.png","grover/user_2.png", "grover/Grover_Bubble_Unicycle_Broken.png", "grover/Grover_Bubble_Unicycle_Broken_Painted.png", "grover/Grover_Bubble_Unicycle_Fixed.png", "grover/Grover_Head_Lh.png", "grover/Grover_Head_Ph.png", "grover/Grover_Helmet_ladybug.png", "grover/Grover_Helmet_Ladybug_withtag.png", "grover/Grover_Helmet_Purple.png", "grover/Grover_Helmet_Purple_withtag.png", "grover/grover_looking.png", "grover/Grover_Paint_Blue.png", "grover/Grover_Paint_Blue_withtag.png", "grover/Grover_Paint_Green.png", "grover/Grover_Paint_Green_withtag.png", "grover/Grover_Unicycle_Silo.png", "grover/Grover_Unicycle_Silo_Bb.png", "grover/Grover_Unicycle_Silo_Bb_Bw.png", "grover/Grover_Unicycle_Silo_Bb_Gw.png", "grover/Grover_Unicycle_Silo_Bw.png", "grover/Grover_Unicycle_Silo_Gb.png", "grover/Grover_Unicycle_Silo_Gb_Bw.png", "grover/Grover_Unicycle_Silo_Gb_Gw.png", "grover/Grover_Unicycle_Silo_Gw.png", "grover/Grover_Wheel_Round.png", "grover/Grover_Wheel_Round_withtag.png", "grover/Grover_Wheel_Rugged.png", "grover/Grover_Wheel_Rugged_withtag.png", "grover/Grover_Helmet_Ladybug_withtag_glow.png", "grover/Grover_Helmet_Purple_withtag_glow.png", "grover/Grover_Paint_Blue_withtag_glow.png", "grover/Grover_Paint_Green_withtag_glow.png", "grover/Grover_Wheel_Round_withtag_glow.png", "grover/Grover_Wheel_Rugged_withtag_glow.png"
				],
				"locked": true,
				"played" : true,
				"totalCoin": 10,
				"coinIcon": "coin.png",
				"coinIconGlow": "coin-glow.png",
				"registerCoin": "coin_half.png",
				"registerCount": "Cash_Register_Number_",
				"elmo": "grover/grover_looking.png",
				"climaxBg": [
					"Background_Outside_Loop.png",
					"48-2.jpg"
				],
				"introAudio": [
					"BS_3.1_66.wav",
					"BS_3.1_70.wav"
				],
				"totalProduct": {
					"itemBg": "41.png",
					"cyclebg": "grover/Background_Bike_Shop_dark_Grover.png",
					"cycle": "grover/Grover_Unicycle_Silo.png",
					"alias" : "Grover_Unicycle_Silo",
					"headAlias" : "Grover_Head",
					"boughtCompleteAudio" : "BS_3.2_87.wav",
					"fixAudio" : "BS_3.2_88.wav",
					"dragAudio" : "BS_3.3_82.wav",
                    "dragAudioDesktop" : "BS_3.3_83.wav",
					"delayAudio" : "BS_3.3_84.wav",
                    "delayAudioDesktop" : "BS_3.3_85.wav",
					"dragCompleteAudio" : "BS_3.3_88.wav",
                    "noSaveAudio": "BS_3.2_87.wav"
				},
				"thoughtBubble": [
					"thought_1.png",
					"thought_2.png",
					"thought_3.png",
					"thought_4.png",
					"thought_5.png",
					"thought_6.png",
					"thought_7.png",
					"thought_8.png",
					"thought_9.png"
				],
				"intro":{
					"user": ["grover/user_1.png","grover/user_2.png"],
					"position": ["left","left", "left", "left", "left"],
					"thoughtContent": ["grover/Grover_Bubble_Unicycle_Broken.png","grover/Grover_Bubble_Unicycle_Broken_Painted.png", "grover/Grover_Bubble_Unicycle_Fixed.png", "grover/Grover_Helmet_Purple.png", "coin_jar.png"],
					"background" : "Background_Outside.png",
					"lastAudioTime": 8000,
					"coinAudio": "BS_3.2_71.wav",
					"register": "BS_2.2_21.wav",
					"tapMobile" : "BS_2.2_22.wav",
					"clickDesktop" : "BS_2.2_23.wav"
				},
				"game": {
					"bgImage": "Background_Bike_Shop_Shelves.png",
					"items": [

						[
							{
								"img": "grover/Grover_Paint_Green_withtag.png",
								"img_glow": "grover/Grover_Paint_Green_withtag_glow.png",
								"alias": "Gb",
								"price": 2,
								"priority": 1,
								"head" : false,
								"bought_img" : "grover/Grover_Paint_Green.png",
								"clickAudio" : "BS_4.2_100.wav",
								"chooseAudio" : "BS_4.2_99.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_3.2_92.wav",
                                "returnItemDesktop": "BS_3.2_93.wav",
                                "delayPlay": ["BS_4.2_99.wav", "BS_4.2_99.wav"]
							},
							{
								"img": "grover/Grover_Paint_Blue_withtag.png",
								"img_glow": "grover/Grover_Paint_Blue_withtag_glow.png",
								"alias": "Bb",
								"price": 3,
								"priority": 1,
								"head" : false,
								"bought_img" : "grover/Grover_Paint_Blue.png",
								"clickAudio" : "BS_4.2_100.wav",
								"chooseAudio" : "BS_4.2_99.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_3.2_92.wav",
                                "returnItemDesktop": "BS_3.2_93.wav",
                                "delayPlay": ["BS_4.2_99.wav", "BS_4.2_99.wav"]
							}
						],
						[
							{
								"img": "grover/Grover_Wheel_Round_withtag.png",
								"img_glow": "grover/Grover_Wheel_Round_withtag_glow.png",
								"alias": "Gw",
								"price": 2,
								"priority": 2,
								"head" : false,
								"bought_img" : "grover/Grover_Wheel_Round.png",
								"clickAudio" : "BS_2.2_36.wav",
								"chooseAudio" : "BS_2.2_35.wav",
                                "payFor" : "BS_2.2_37.wav",
                                "payForDesktop" : "BS_2.2_38.wav",
                                "returnItem" : "BS_2.2_50.wav",
                                "returnItemDesktop": "BS_2.2_51.wav",
                                "delayPlay": ["BS_2.2_50.wav", "BS_2.2_51.wav"]
							},
							{
								"img": "grover/Grover_Wheel_Rugged_withtag.png",
								"img_glow": "grover/Grover_Wheel_Rugged_withtag_glow.png",
								"alias": "Bw",
								"price": 3,
								"priority": 2,
								"head" : false,
								"bought_img" : "grover/Grover_Wheel_Rugged.png",
								"clickAudio" : "BS_2.2_36.wav",
								"chooseAudio" : "BS_2.2_35.wav",
                                "payFor" : "BS_2.2_37.wav",
                                "payForDesktop" : "BS_2.2_38.wav",
                                "returnItem" : "BS_2.2_50.wav",
                                "returnItemDesktop": "BS_2.2_51.wav",
                                "delayPlay": ["BS_2.2_50.wav", "BS_2.2_51.wav"]
							}
						],
						[
							{
								"img": "grover/Grover_Helmet_Ladybug_withtag.png",
								"img_glow": "grover/Grover_Helmet_Ladybug_withtag_glow.png",
								"alias": "Lh",
								"price": 4,
								"priority": 0,
								"head" : true,
								"bought_img" : "grover/Grover_Helmet_ladybug.png",
								"clickAudio" : "BS_3.2_76.wav",
								"chooseAudio" : "BS_3.2_75.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_3.2_80.wav",
                                "returnItemDesktop": "BS_3.2_81.wav",
                                "delayPlay": ["BS_3.2_80.wav", "BS_3.2_81.wav"]
							},
							{
								"img": "grover/Grover_Helmet_Purple_withtag.png",
								"img_glow": "grover/Grover_Helmet_Purple_withtag_glow.png",
								"alias": "Ph",
								"price": 3,
								"priority": 0,
								"head" : true,
								"bought_img" : "grover/Grover_Helmet_Purple.png",
								"clickAudio" : "BS_3.2_76.wav",
								"chooseAudio" : "BS_3.2_75.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_3.2_80.wav",
                                "returnItemDesktop": "BS_3.2_81.wav",
                                "delayPlay": ["BS_3.2_80.wav", "BS_3.2_81.wav"]
							}
						]
					]
				},
                "payoff": 'grover_payoff.mp3',
                "fixDelay" : ["BS_3.3_86.wav", "BS_3.3_87.wav"],
                "payDelay" : ["BS_2.2_52.wav","BS_2.2_53.wav"],
                "finishAddCoin" : "BS_2.2_54.wav"
			},
			{
				"image": "Choose_Cookie_Locked.png",
				"name" : "cookie",
				"image_active": "Choose_Cookie.png",
				"audio_assets" : [
					'BS_5.0_120.wav', 'BS_5.0_121.wav', 'BS_5.0_122.wav', 'BS_5.0_123.wav', 'BS_5.0_124.wav', 'BS_5.0_125.wav', 'BS_5.0_126.wav', 'BS_5.0_127.wav', 'BS_5.0_128.wav', 'BS_5.0_129.wav', 'BS_2.2_37.wav', 'BS_2.2_38.wav', 'BS_5.0_130.wav', 'BS_5.0_131.wav', 'BS_5.0_132.wav', 'BS_5.0_133.wav', 'BS_5.0_134.wav', 'BS_5.0_135.wav', 'BS_5.0_136.wav', 'BS_5.0_137.wav', 'BS_5.0_138.wav', 'BS_5.0_139.wav', "BS_4.1_92.wav", "BS_4.1_96.wav", "BS_4.2_97.wav", "BS_2.2_21.wav", "BS_2.2_22.wav", "BS_2.2_23.wav", "BS_4.2_99.wav", "BS_4.2_100.wav", "BS_2.2_36.wav", "BS_2.2_35.wav", "BS_4.2_102.wav", "BS_4.3_108.wav", "BS_4.3_109.wav", "BS_4.2_104.wav", "BS_4.2_103.wav", "BS_4.3_110.wav", "BS_4.3_111.wav", "BS_4.4_112.wav", "BS_3.2_76.wav", "coin_return.mp3", "BS_3.2_75.wav", "BS_2.2_26.wav", "BS_2.2_27.wav", "BS_2.2_37.wav", "BS_2.2_38.wav", "BS_3.2_93.wav", "BS_3.2_92.wav", "BS_3.2_80.wav", "BS_3.2_81.wav", "BS_2.2_50.wav", "BS_2.2_51.wav", "cookie_payoff.mp3", "BS_4.2_103_alt.wav", "BS_2.2_52.wav", "BS_2.2_53.wav", "BS_2.2_54.wav", "BS_3.3_86.wav", "BS_3.3_87.wav"
				],
				"image_assets" : [
					"cookie/user_1.png","cookie/user_2.png", "cookie/Background_Bike_Shop_dark_Cookie.png", "cookie/Cookie_Bubble_Scooter_Broken.png", "cookie/Cookie_Bubble_Scooter_Broken_Blue.png", "cookie/Cookie_Bubble_Scooter_Fixed.png", "cookie/Cookie_Head_Ch.png", "cookie/Cookie_Head_Yh.png", "cookie/Cookie_Helmet_Chips.png", "cookie/Cookie_Helmet_Chips_withtag.png", "cookie/Cookie_Helmet_Yellow.png", "cookie/Cookie_Helmet_Yellow_withtag.png", "cookie/cookie_looking.png", "cookie/Cookie_Paint_Blue.png", "cookie/Cookie_Paint_Blue_withtag.png", "cookie/Cookie_Paint_Green.png", "cookie/Cookie_Paint_Green_withtag.png", "cookie/Cookie_Scooter_Silo.png", "cookie/Cookie_Scooter_Silo_Bs.png", "cookie/Cookie_Scooter_Silo_Bs_Gw.png", "cookie/Cookie_Scooter_Silo_Bs_Ow.png", "cookie/Cookie_Scooter_Silo_Gs.png", "cookie/Cookie_Scooter_Silo_Gs_Gw.png", "cookie/Cookie_Scooter_Silo_Gs_Ow.png", "cookie/Cookie_Scooter_Silo_Gw.png", "cookie/Cookie_Scooter_Silo_Ow.png", "cookie/Cookie_Wheels_Green.png", "cookie/Cookie_Wheels_Green_withtag.png", "cookie/Cookie_Wheels_Orange.png", "cookie/Cookie_Wheels_Orange_withtag.png", "cookie/Cookie_Helmet_Chips_withtag_glow.png", "cookie/Cookie_Helmet_Yellow_withtag_glow.png", "cookie/Cookie_Paint_Blue_withtag_glow.png", "cookie/Cookie_Paint_Green_withtag_glow.png", "cookie/Cookie_Wheels_Green_withtag_glow.png", "cookie/Cookie_Wheels_Orange_withtag_glow.png"
				],
				"locked": true,
				"played" : true,
				"totalCoin": 10,
				"coinIcon": "coin.png",
				"coinIconGlow": "coin-glow.png",
				"registerCoin": "coin_half.png",
				"registerCount": "Cash_Register_Number_",
				"elmo": "cookie/cookie_looking.png",
				"climaxBg": [
					"Background_Outside_Loop.png",
					"48-2.jpg"
				],
				"introAudio": [
					"BS_4.1_92.wav",
					"BS_4.1_96.wav"
				],
				"totalProduct": {
					"itemBg": "41.png",
					"cyclebg": "cookie/Background_Bike_Shop_dark_Cookie.png",
					"cycle": "cookie/Cookie_Scooter_Silo.png",
					"alias" : "Cookie_Scooter_Silo",
					"headAlias" : "Cookie_Head",
					"boughtCompleteAudio" : "BS_4.2_103.wav",
					"fixAudio" : "BS_4.2_104.wav",
					"dragAudio" : "BS_4.3_108.wav",
                    "dragAudioDesktop" : "BS_4.3_109.wav",
					"delayAudio" : "BS_4.3_110.wav",
                    "delayAudioDesktop" : "BS_4.3_111.wav",
					"dragCompleteAudio" : "BS_4.4_112.wav",
                    "noSaveAudio": "BS_4.2_103_alt.wav"
				},
				"thoughtBubble": [
					"thought_1.png",
					"thought_2.png",
					"thought_3.png",
					"thought_4.png",
					"thought_5.png",
					"thought_6.png",
					"thought_7.png",
					"thought_8.png",
					"thought_9.png"
				],
				"intro":{
					"user": ["cookie/user_1.png","cookie/user_2.png"],
					"position": ["left","left", "left", "left", "left"],
					"thoughtContent": ["cookie/Cookie_Bubble_Scooter_Broken.png","cookie/Cookie_Bubble_Scooter_Broken_Blue.png", "cookie/Cookie_Bubble_Scooter_Fixed.png", "cookie/Cookie_Helmet_Yellow.png", "coin_jar.png"],
					"background" : "Background_Outside.png",
					"lastAudioTime": 9000,
					"coinAudio": "BS_4.2_97.wav",
					"register": "BS_2.2_21.wav",
					"tapMobile" : "BS_2.2_22.wav",
					"clickDesktop" : "BS_2.2_23.wav"
				},
				"game": {
					"bgImage": "Background_Bike_Shop_Shelves.png",
					"items": [

						[
							{
								"img": "cookie/Cookie_Paint_Green_withtag.png",
								"img_glow": "cookie/Cookie_Paint_Green_withtag_glow.png",
								"alias": "Gs",
								"price": 1,
								"priority": 1,
								"head" : false,
								"bought_img" : "cookie/Cookie_Paint_Green.png",
								"clickAudio" : "BS_4.2_100.wav",
								"chooseAudio" : "BS_4.2_99.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_3.2_92.wav",
                                "returnItemDesktop": "BS_3.2_93.wav",
                                "delayPlay": ["BS_4.2_99.wav", "BS_4.2_99.wav"]
							},
							{
								"img": "cookie/Cookie_Paint_Blue_withtag.png",
								"img_glow": "cookie/Cookie_Paint_Blue_withtag_glow.png",
								"alias": "Bs",
								"price": 3,
								"priority": 1,
								"head" : false,
								"bought_img" : "cookie/Cookie_Paint_Blue.png",
								"clickAudio" : "BS_4.2_100.wav",
								"chooseAudio" : "BS_4.2_99.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_3.2_92.wav",
                                "returnItemDesktop": "BS_3.2_93.wav",
                                "delayPlay": ["BS_4.2_99.wav", "BS_4.2_99.wav"]
							}

						],
						[
							{
								"img": "cookie/Cookie_Helmet_Chips_withtag.png",
								"img_glow": "cookie/Cookie_Helmet_Chips_withtag_glow.png",
								"alias": "Ch",
								"price": 4,
								"priority": 0,
								"head" : true,
								"bought_img" : "cookie/Cookie_Helmet_Chips.png",
								"clickAudio" : "BS_3.2_76.wav",
								"chooseAudio" : "BS_3.2_75.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_3.2_80.wav",
                                "returnItemDesktop": "BS_3.2_81.wav",
                                "delayPlay": ["BS_3.2_80.wav", "BS_3.2_81.wav"]
							},
							{
								"img": "cookie/Cookie_Helmet_Yellow_withtag.png",
								"img_glow": "cookie/Cookie_Helmet_Yellow_withtag_glow.png",
								"alias": "Yh",
								"price": 2,
								"priority": 0,
								"head" : true,
								"bought_img" : "cookie/Cookie_Helmet_Yellow.png",
								"clickAudio" : "BS_3.2_76.wav",
								"chooseAudio" : "BS_3.2_75.wav",
                                "payFor" : "BS_2.2_26.wav",
                                "payForDesktop" : "BS_2.2_27.wav",
                                "returnItem" : "BS_3.2_80.wav",
                                "returnItemDesktop": "BS_3.2_81.wav",
                                "delayPlay": ["BS_3.2_80.wav", "BS_3.2_81.wav"]
							}
						],
						[
							{
								"img": "cookie/Cookie_Wheels_Green_withtag.png",
								"img_glow": "cookie/Cookie_Wheels_Green_withtag_glow.png",
								"alias": "Gw",
								"price": 3,
								"priority": 2,
								"head" : false,
								"bought_img" : "cookie/Cookie_Wheels_Green.png",
								"clickAudio" : "BS_2.2_36.wav",
								"chooseAudio" : "BS_2.2_35.wav",
                                "payFor" : "BS_2.2_37.wav",
                                "payForDesktop" : "BS_2.2_38.wav",
                                "returnItem" : "BS_2.2_50.wav",
                                "returnItemDesktop": "BS_2.2_51.wav",
                                "delayPlay": ["BS_2.2_50.wav", "BS_2.2_51.wav"]
							},
							{
								"img": "cookie/Cookie_Wheels_Orange_withtag.png",
								"img_glow": "cookie/Cookie_Wheels_Orange_withtag_glow.png",
								"alias": "Ow",
								"price": 5,
								"priority": 2,
								"head" : false,
								"bought_img" : "cookie/Cookie_Wheels_Orange.png",
								"clickAudio" : "BS_2.2_36.wav",
								"chooseAudio" : "BS_2.2_35.wav",
                                "payFor" : "BS_2.2_37.wav",
                                "payForDesktop" : "BS_2.2_38.wav",
                                "returnItem" : "BS_2.2_50.wav",
                                "returnItemDesktop": "BS_2.2_51.wav",
                                "delayPlay": ["BS_2.2_50.wav", "BS_2.2_51.wav"]
							}
						]
					]
				},
                "payoff": 'cookie_payoff.mp3',
                "payDelay" : ["BS_2.2_52.wav","BS_2.2_53.wav"],
                "fixDelay" : ["BS_3.3_86.wav", "BS_3.3_87.wav"],
                "finishAddCoin" : "BS_2.2_54.wav"
            },

		],
		"offset": 20,
		"bgImage": "Title_Background.png",
		"levelAudio" : [
			{
				'mobile': 'BS_1.1_11.wav',
				'desktop': 'BS_1.1_12.wav'
			}
		]
	}
}
