import Levels from "../../../../src/assets/js/components/levels.js";
import sound from "../../../../src/assets/js/components/sound.js";
import { Animate , Anim }from "../../../../src/assets/js/components/animation.js";
import { Stringify } from "../../../../src/assets/js/components/utils.js";
import Device from "../../../../src/assets/js/components/device.js";
import Loader from "../../../../src/assets/js/components/loader.js";
/**
 * { item_description }
 */
export default class Game {
	/**
	 * { Initialize data }
	 *
	 * @param      {<Object>}  o         { CONFIG Object}
	 * @param      {<Object>}  renderer  The renderer
	 * @param      {<Object>}  level     The level
	 */
	constructor(o, renderer, level) {
		this.opt = o;
		this.container = new PIXI.Container();
		this.renderer = renderer;
		this.currentLevel = level;
		this.homeIcon = '';
		this.play = new PIXI.Container();
		this.game_stage = new PIXI.Container();
		this.game = {};
		this.topCoins = new PIXI.Container();
		this.topCoinsGlow = new PIXI.Container();
		this.overLay = [];
		this.basketArr = [];
		this.selectedItem = {};
		this.animate = null;
		this.frameAnim = null;
		this.draggedItem = [];
		this.animArr = 0;
		this.timeouts = [];
		this.touchtimeinterval = null;
		this.windowresized = false
        //this.animationTimeout = null;
        this.selectedItemPrice = 0;

        window.addEventListener('resize', this.resize.bind(this), false);
        this.introResize = true;
        this.shopResize = false;
        this.buildResize = false;
        this.payoffResize = false;
		return this;
	}
	/**
	 * { start playing the intro with animation }
	 */
	playIntro() {
		this.frameAnimate();
		sound.stop();
		this.thoughts = [];
		this.slides = [];
		/**
		 * Play intro sound
		 */
		let plyCount = 0;
		let frameObjArr = [];
		let loader = new Loader('loading');
		// Intro Object for each level
		this.intro = this.opt.levels.group[this.currentLevel].intro;

		this.addBackground();

		this.addSkipButton();
		// update view
		this.updateView();
		let thought;
        let playThoughtFrames = () => {
            /**
             * [thought creating thought object]
             * @type {[Object]}
             */
            let thought_frames = [];
            for(let k = 0, thb = this.opt.levels.group[this.currentLevel].thoughtBubble;k < thb.length;k += 1) {
                thought_frames.push(PIXI.Texture.fromImage(this.opt.baseImageUrl + thb[k]));
            }
            thought = new PIXI.extras.MovieClip(thought_frames);
            thought.height = (this.opt.baseHeight) / 2;
            if(new Device().isMobile()) {
                thought.height = (this.opt.baseHeight) / 2.2;
            }
            thought.width = thought._height / (thought.texture.height / thought.texture.width);
            thought.x = (this.opt.baseWidth / 3.02958);
            /*if(intro.position[i] == 'right') {
             thought.x = (this.opt.baseWidth / 2) - (thought._width);
             }*/
            thought.y = 20;
            thought.animationSpeed = 0.7;
            thought.play();
            this.play.addChild(thought);
            this.container.addChild(this.play);

        }


		let playSlides = (i) => {

			/*if(i <= 2) {
				if(i > 0) {
					this.slides[0].stop();
					this.slides[0].visible = false;
					this.play.removeChild(0, this.slides[0]);
					this.slides = [];
				}
                let framesObj = frameObjArr[i];
		    	let frames = [];
		    	for (let key in framesObj) {
		          frames.push(PIXI.Texture.fromImage(key));
		        }

		        slide = new PIXI.extras.MovieClip(frames);
		    	if(this.opt.levels.group[this.currentLevel].name == 'cookie') {
                    slide.height = this.opt.baseHeight / 1.5836;
                } else {
                    slide.height = this.opt.baseHeight / 1.3836;
                }

                if(new Device().isMobile()) {
                    if(this.opt.levels.group[this.currentLevel].name == 'cookie') {
                        slide.height = this.opt.baseHeight / 1.7836;
                    } else {
                        slide.height = this.opt.baseHeight / 1.7836;
                    }
                }
				slide.width = slide._height / (slide.texture.height / slide.texture.width);
				slide.x = (this.opt.baseWidth / 2.4) - (slide._width);
                if(this.opt.levels.group[this.currentLevel].name == 'cookie') {
                    slide.x = (this.opt.baseWidth / 2.25) - (slide._width);
                }
				if(intro.position[i] == 'right') {
					slide.x = (this.opt.baseWidth / 3);
				}
				slide.y = this.opt.baseHeight - slide._height - 20;
				slide.animationSpeed = 0.4;
				slide.loop = false;
				slide.play();

				this.slides.push(slide);

		        this.play.addChild(slide);
			}
			*//**
			 * [thoughtContent what the character is thinking]
			 * @type {[Object]}
			 */
			let thoughtContent = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.intro.thoughtContent[i]);
			thoughtContent.height = this.opt.baseHeight / 2.95;
			thoughtContent.width = thoughtContent._height / (thoughtContent.texture.height / thoughtContent.texture.width);
			thoughtContent.x = thought.position.x + ((thought._width - thoughtContent._width) / 2) + 30;
			//console.log(thought._height, thoughtContent.texture.height);
			thoughtContent.y = thought.position.y + ((thought._height - thoughtContent._height) / 2);
			/**
			 * [if the first slide is over then render the second child and remove the previous one]
			 * @param  {[number]} i >             0 [current index]
			 * @return {[Object]}   [description]
			 */
			if(i > 0) {
				//console.log(thoughts[i - 1]);
				this.play.removeChild(0, this.thoughts[0]);
				this.thoughts = [];
			}
			/**
			 * Adding items to the container
			 */
			this.thoughts.push(thoughtContent);
			this.play.addChild(thoughtContent);
			/**
			 * Updating the view
			 */
			this.updateView();

		}

		let slideImages = (countImage) => {
            if(this.slides.length > 0) {
                this.slides[0].visible = 0;
                this.slides[0].destroy();
                this.slides.length = 0;
            }
            /**
             * [slide creating one slide]
             * @type {[Object]}
             */
            let slide = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.intro.user[countImage]);
            if(this.opt.levels.group[this.currentLevel].name == 'cookie') {
                slide.height = this.opt.baseHeight / 1.7836;
            } else {
                slide.height = this.opt.baseHeight / 1.7836;
            }

            slide.width = slide._height / (slide.texture.height / slide.texture.width);
            slide.x = (this.opt.baseWidth / 2.4) - (slide._width);
            if(this.opt.levels.group[this.currentLevel].name == 'cookie') {
                slide.x = (this.opt.baseWidth / 2.25) - (slide._width);
            }
            if(this.intro.position[countImage] == 'right') {
                slide.x = (this.opt.baseWidth / 3);
            }
            slide.y = this.opt.baseHeight - slide._height - 20;

            this.slides.push(slide);

            this.play.addChild(slide);

        }

		let recursivePlay = () => {
			//console.log('Count: ', plyCount);
			sound.play((this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].introAudio[plyCount]), () => {
				//console.log("teststs");
				plyCount++;
				if(plyCount < this.opt.levels.group[this.currentLevel].introAudio.length) {
					//console.log('recursivePlay', plyCount);
                    slideImages(plyCount);
					return recursivePlay(plyCount);
				} else {
					this.play.removeChildren(0, this.play.children.length);
					this.container.removeChild(this.intro_bg);
                    this.container.removeChild(this.skip_button);
					this.updateView();
                    window.removeEventListener('resize', this.resize);
                    this.introResize = false;
                    this.shopResize = true;
                    window.addEventListener('resize', this.resizeGame.bind(this), false);
					this.startGame();
					//console.log('recursivePlay', plyCount);
					return true;
				}

			});
		}
		//Show loader until animation object loads

        let afterAssetLoad = () => {
            for(let f = 0; f < this.intro.user.length; f += 1) {
                frameObjArr.push(this.d.resources[this.opt.baseImageUrl + this.intro.user[f]].data.frames);
            }
            /**
             * Checking the totol intro audio time
             * Dividing the total time with the total content
             * getting the indivisual time for each slide then putting it into the setimeout function
             * to play slides with the audio
             * @type {number}
             */
            let totalTime = 0;
            let promises = [];
            var promise = new Promise((resolve, reject) => {
                sound.duration(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].introAudio[0], (time) => {
                    resolve(time);
                });
            });
            promises.push(promise);

            Promise.all(promises).then(res => {
                console.log(res);
                for(let tl = 0;tl < res.length;tl++) {
                    totalTime = totalTime + res[tl];
                }
                totalTime = totalTime + 0.5;
                let timeSlot = (totalTime / this.opt.levels.group[this.currentLevel].intro.thoughtContent.length);
                for(let tm = 0;tm < this.opt.levels.group[this.currentLevel].intro.thoughtContent.length; tm++) {
                    let tmout = setTimeout((cnt) => {
                        playSlides(cnt);
                    }, ((timeSlot * tm) * 1000), tm);
                    this.timeouts.push(tmout);
                }
                playThoughtFrames();
                /**
                 * Recursive play of audios
                 */
                recursivePlay();

                slideImages(0);
            });
        }

        if(!this.windowresized) {
            loader.show();

            var assetLoader = new PIXI.loaders.Loader();
            for(let m = 0;m < this.intro.user.length; m += 1) {
                assetLoader.add(this.opt.baseImageUrl + this.intro.user[m]);
            }
            assetLoader.load(d => {
                this.d = d;
                afterAssetLoad();
            });
        } else {
            afterAssetLoad();
        }

	}
	/**
	 * [addTopCoins Add coins to top bar]
	 */
	addTopCoins() {
		let isClickEvent = false;
        let isAnimating = false;
        let startDrag = false;
		/**
		 * [Properties properties for top coins container]
		 * @type {[Object]}
		 */

        this.topCoins.y = this.opt.baseHeight / 36.9189;
		this.topCoins.x = this.opt.baseWidth / 10.4545;
		this.topCoins.height = this.opt.baseHeight / 8.3518;


        this.topCoinsGlow.y = this.opt.baseHeight / 36.9189;
        this.topCoinsGlow.x = this.opt.baseWidth / 10.4545;
		this.topCoinsGlow.height = this.opt.baseHeight / 8.3518;
		this.topCoinsGlow.visible = false;

		this.registerCounterVal = 0;

		/**
		 * [for adding each coins]
		 */
		for(let i = 0;i < this.opt.levels.group[this.currentLevel].totalCoin;i += 1) {
			/**
			 * [data coin data]
			 * @type {[Object]}
			 */
			let data = null;
			/**
			 * [isDrag to check drag is enabled or not]
			 * @type {Boolean}
			 */
			let isDrag = false;
			/**
			 * [coin coin image texture]
			 * @type {Object}
			 */
			let coin = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].coinIcon);
			let coinGlow = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].coinIconGlow);

            //Coin properties
            coin.x = (i * (this.topCoins._height + 8)) + 10;
            //Coin properties
            coinGlow.x = (i * (this.topCoins._height + 8)) + 10;
            coin.height = this.opt.baseHeight / 8.3518;
            coinGlow.height = this.opt.baseHeight / 8.3518;
			//console.log(coin.rotation);
			//coin.rotation = 2;
			// if width is less than 768 then height is diffrent

			coin.width = coin._height / (coin.texture.height / coin.texture.width);

			coinGlow.width = coinGlow._height / (coinGlow.texture.height / coinGlow.texture.width);
			//coin.scale.x = 0.2;
			//coin.scale.y = 0.2;
			coin.interactive = false;
			coin.buttonMode = true;
			coin.defaultCursor = "pointer";

			/**
			 * [description]
			 * @param  {[Object]} event [event object]
			 * @return {[object]}       [context]
			 */
			let coinDragStart = (event) => {
				// Make drag true and assign data
                if(isAnimating) {
                    return;
                }
                if(startDrag) {
                    return;
                }
                sound.stop();
				event.stopPropagation();
				isDrag = true;
				data = event.data;
				if(i == 0) {
					Anim.animateRegisterBox(this.coinContainer, this.registerCount);
				}
			}
			let coinDragMove = () => {
                if(isAnimating) {
                    return;
                }
			    if(isDrag) {
					var newPosition = data.getLocalPosition(coin.parent);
					coin.position.x = newPosition.x - (coin._width);
			        coin.position.y = newPosition.y - (coin._height);
			        this.updateView();
				}

			}
			let coinDragEnd = () => {
                if(isAnimating) {
                    return;
                }
			    isDrag = false;
				data = null;
				//console.log(coin.position.x, this.coinContainer.position.x);
				//console.log(coin.position.y, this.coinContainer.position.y);
				//let coinX = Math.floor(coin.position.x + (this.topCoins.position.x));
				//let coinY = Math.floor(coin.position.y + (this.topCoins.position.y));

				//if((coinX > Math.floor(this.coinContainer.position.x)) && (coinY > Math.floor(this.coinContainer.position.y))) {
				setTimeout(() => {
				    //console.log(isClickEvent);
				    if(!isClickEvent) {
                        dragEndOnCointainer();
                    }
                    //} else {
                        //coin.visible = true;
                    //}
                    coin.x = (i * (this.topCoins._height + 10)) + 10;
                    coin.y = 0;
                    this.updateView();
				}, 302);
            }

			let dragEndOnCointainer = () => {
			    //clear touch timeout if any
                this.flushTimeOuts();
				this.registerCount.count = parseInt(this.registerCount.count) + 1;
                this.registerCount.visible = true;
				this.registerCount.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].registerCount + (parseInt(this.registerCounterVal) + 1) + '.png');
				//Play the sound for register count
				//sound.stop();
				//console.log(this.opt.baseMediaUrl + 'BS_5.0_13' + this.registerCounterVal +'.m4a');
				sound.play(this.opt.baseMediaUrl + 'BS_5.0_13' + this.registerCounterVal +'.wav');
				// text of the register
				let regText = parseInt(this.registerCount.count);
				//console.log(regText);
				// Price of the item
				let itemText = parseInt(this.opt.levels.group[this.currentLevel].game.items[this.selectedItem.row][this.selectedItem.col].price);
				//console.log(itemText, this.basket);
				for(let bkt = 0; bkt < this.basketArr.length; bkt += 1) {
					//console.log(this.basket_price[bkt].price.text);
					if(this.basketArr[bkt]) {
						itemText += parseInt(this.basketArr[bkt].price);
					}
				}
				//console.log(regText, itemText, this.basketArr);
				if(regText == itemText) {
					/*this.overLay.map((ol) => {
						ol.visible = true;
					});*/
					//this.registerCount.visible = false;

					this.addToBasket(this.game.items[this.selectedItem.row][this.selectedItem.col], this.selectedItem.row, this.selectedItem.col);

					this.game.items[this.selectedItem.row][this.selectedItem.col].visible = false;
					// Reseting the isZoomed value of selected item
					this.game.items[this.selectedItem.row][this.selectedItem.col].base.isZoomed = false;
					// update the register
					coin.visible = false;
					Anim.moveCoins(this.topCoins.children , i, () => {
						//console.log('console.log');
						coin.visible = true;
                        isAnimating = false;
                        startDrag = false;
						this.updateRegister();
					});

					if(this.selectedItem.row != this.overLay.length - 1) {
						//this.playItemAudio(this.opt.levels.group[this.currentLevel].game.itemsAudio[this.selectedItem.row + 1]);
						this.updateOverlay();

						for( let ad = 0; ad < this.overLay.length; ad += 1) {
							if(this.overLay[ad].visible == false) {
								setTimeout(() => {
									sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[ad][0].chooseAudio);
								}, 500);
								break;
							}
						}
					}
                    this.selectedItem = {};
					for(let k = 0;k < this.topCoins.children.length;k += 1) {
						this.topCoins.children[k].interactive = false;
					}
                    this.registerCounterVal = 0;
				} else {
                    this.registerCounterVal++;
					this.registerCount.visible = true;
                    this.touchtimeinterval = setTimeout(() => {
                        sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].finishAddCoin);
                    }, 8000);
                    this.timeouts.push(this.touchtimeinterval);

					//this.regCoin.visible = true;
					coin.visible = false;
					Anim.moveCoins(this.topCoins.children , i, () => {
						coin.visible = true;
                        isAnimating = false;
                        startDrag = false;
						for(let rc = 0;rc < this.registerCount.count; rc += 1) {
							this.topCoins.children[rc].visible = false;
						}
					});
				}
			}

			let coinClickItemSelect = (event) => {
                /**
                 * Add google analytics on Coin click
                 */
                if(new Device().isMobile()) {
                    ga('send', 'event', 'Bike Shop', 'touch_input', 'Purchase_Tap_Mobile');
                } else {
                    ga('send', 'event', 'Bike Shop', 'mouse_input', 'Purchase_Click_Desktop');
                }
                this.flushTimeOuts();

                if(!isAnimating) {
                    isAnimating = true;
                    isClickEvent = true;
                    event.stopPropagation();
                    sound.stop();
                    coin.visible = false;
                    sound.play(this.opt.baseMediaUrl + 'coin_return.mp3');
                    Anim.coinClickAnimation(this.opt, this.container, {x: (coin.position.x + coin._width), y: coin.position.y}, { x: (this.opt.baseWidth / 1.3368), y: (this.opt.baseHeight / 1.48)}, 1000, () => {
                        dragEndOnCointainer();
                        isClickEvent = false;
                        isAnimating = false;
                        if(new Device().isMobile()) {
                            this.touchtimeinterval = setTimeout(() => {
                                sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].payDelay[0]);
                            }, 8000);
                        } else {
                            this.touchtimeinterval = setTimeout(() => {
                                sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].payDelay[1]);
                            }, 8000);
                        }
                        this.timeouts.push(this.touchtimeinterval);

                    });
                } else {
                    return ;
                }

			}
			/*coin.on('touchstart', coinDragStart);
			coin.on('mousedown', coinDragStart);

			coin.on('touchmove', coinDragMove);
			coin.on('mousemove', coinDragMove);

			coin.on('touchend', coinDragEnd);
			coin.on('mouseup', coinDragEnd);

			coin.on('mouseupoutside', coinDragEnd)
			coin.on('touchendoutside', coinDragEnd);*/

			coin.on('click', coinClickItemSelect);
			coin.on('tap', coinClickItemSelect);

			/*coin.height = 100;
			coin.width = 100;*/
			this.topCoins.addChild(coin);
			this.topCoinsGlow.addChild(coinGlow);

		}

		/**
		 * Adding coins to the top container
		 */
		this.game_stage.addChild(this.topCoins);
		this.game_stage.addChild(this.topCoinsGlow);
		/**
		 * Updating the view
		 */
		this.updateView();
		//this.animateCoins();
	}
	/**
	 * [addHomeIcon add home icon to the intro level]
	 */
	addHomeIcon() {
	    if(this.homeIcon) {
            this.container.removeChild(this.homeIcon);
        }
		/**
		 * [homeIcon home icon image and set other properties]
		 * @type {[Object]}
		 */
		this.homeIcon = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.homeIcon);
		//setting height for different resolution devices
        this.homeIcon.height = this.opt.baseHeight / 8.5714;
		this.homeIcon.width = this.homeIcon._height / (this.homeIcon.texture.height / this.homeIcon.texture.width);
		this.homeIcon.x = this.opt.baseHeight / 44.7647;
		this.homeIcon.y = this.opt.baseHeight / 44.7647;
        if(new Device().isMobile()) {
            this.homeIcon.y = this.opt.baseHeight / 54.7647;
        }
		this.homeIcon.interactive = true;
		this.homeIcon.buttonMode = true;
		this.homeIcon.defaultCursor = "pointer";
		/**
		 * [Add click or tap events to the home icon]
		 * @return {[type]} [description]
		 */
		this.homeIcon.click = this.homeIcon.tap = () => {
            /**
             * Add google analytics on home click
             */
            ga('send', 'event', 'Bike Shop', 'home_clicked', 'Home');

            this.homeButtonClickAction();
		}
		/**
		 * Add icon to the container
		 */
		this.container.addChild(this.homeIcon);
	}
	/**
	 * [setHomeIconIndex set the z-index of home icon]
	 */
	setHomeIconIndex() {
		this.container.setChildIndex(this.homeIcon, this.container.children.length - 1);
	}
	/**
	 * [startGame game starts here]
	 * @return {[Object]} [returns current context]
	 */
	startGame() {
	    // DEstroy skip button
        this.skip_button.interactive = false;
        this.skip_button.visible = false;
        //this.skip_button.destroy();
		/**
		 * [background game background]
		 * @type {[Object]}
		 */
		this.game.background = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].game.bgImage);
		/**
		 * [Properties setting game background]
		 * @type {[Object]}
		 */
		this.game.background.height = this.opt.baseHeight;
		this.game.background.width = this.opt.baseWidth;
		this.game.background.x = 0;
		this.game.background.y = 0;
		/**
		 * Adding game background and fake background to the main container
		 */
		this.game_stage.addChild(this.game.background);
		/**
		 * Add elmo icon
		 */
		this.elmo = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].elmo);
		if(this.opt.levels.group[this.currentLevel].name == 'cookie') {
            this.elmo.height = this.opt.baseHeight / 2.37488;
        } else {
            this.elmo.height = this.opt.baseHeight / 2.97488;
        }

		this.elmo.width = this.elmo.height / (this.elmo.texture.height / this.elmo.texture.width);
        this.elmo.x = (this.opt.baseWidth / 8.1269) - (this.elmo._width / 2);
        this.elmo.y = (this.opt.baseHeight / 1.1978) - (this.elmo._height);
		this.game_stage.addChild(this.elmo);

        /**
         * Add yellow background to the coin
         */
        this.coin_bg = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + 'coin_bg.png');
        this.coin_bg.x = 0;// this.opt.baseWidth / 60.3508;
        this.coin_bg.y = 0;
        this.coin_bg.height = this.opt.baseHeight / 5.5;
        this.coin_bg.width = this.opt.baseWidth / 1.0199;
        this.game_stage.addChild(this.coin_bg);

        /**
         * [items Items array]
		 * @type {Array}
		 */
		this.game.items = [];
		/**
		 * [item_container shelf container]
		 * @type {Object}
		 */
		this.item_container = new PIXI.Container();
		/**
		 * [Properties setting shelf container properties]
		 * @type {[Object]}
		 */
		this.item_container.height = this.opt.baseHeight / 1.58;
		this.item_container.width = this.opt.baseWidth / 2.3;
		this.item_container.x = this.opt.baseWidth / 4.37;
		this.item_container.y = this.opt.baseHeight / 5.122;
		/**
		 * [basket basket container]
		 * @type {Object}
		 */
		this.basket = new PIXI.Container();
		/**
		 * [Properties basket properties]
		 * @type {[Object]}
		 */
		this.basket.height = this.opt.baseHeight - (this.opt.baseHeight / 1.19133);
		this.basket.width = this.opt.baseWidth / 1.4;
		this.basket.x = this.opt.baseWidth / 21.3333;
		this.basket.y = this.opt.baseHeight / 1.19133;
		/**
		 * Adding item container to parent stage
		 */
		this.game_stage.addChild(this.item_container);
		this.game_stage.addChild(this.basket);
		/**
		 * [item basket item]
		 * @type {Array}
		 */
		this.basket.item = [];
		/**
		 * [cell_height one row height]
		 * @type {[number]}
		 */
		let cell_height = (this.item_container._height / this.opt.levels.group[this.currentLevel].game.items.length);
		/**
		 * [cell_width one item width]
		 * @type {[number]}
		 */
		let cell_width = cell_height;
		//cell_height = cell_height;
		/**
		 * [adding each item to the container]
		 * @param  {Function} (item, index)        [description]
		 * @return {[Object]}          [returns the added item]
		 */
		this.opt.levels.group[this.currentLevel].game.items.map((item, index) => {
			/**
			 * [item_arr spliting the items array string]
			 * @type {[Array]}
			 */
			let item_arr = item;
			this.game.items[index] = [];
			// Looping over item_array
			item_arr.map((it, i) => {
				/**
				 * Defining container for each item
				 */
				this.game.items[index][i] = new PIXI.Container();

				this.game.items[index][i].width = (this.item_container._width / 2) - 20;
				this.game.items[index][i].height = this.opt.baseHeight / 4.9253;
				this.game.items[index][i].x = (i * this.game.items[index][i]._width);
				this.game.items[index][i].y = index * this.game.items[index][i]._height;
				/**
				 * [base item image]
				 * @type {[Object]}
				 */
				let base = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + it.img);
				//base.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + it.img);
				/**
				 * [Properties setting the item image properties]
				 * @type {[Object]}
				 */
				//console.log(base.texture.height, base.texture.width);
				/*base.scale.x = 0.5;
				base.scale.y = 0.5;*/
				let ratio = base.texture.height / base.texture.width;
				base.width = this.opt.baseWidth / 7.3528;
				base.height = base._width * ratio;
				base.x = (this.game.items[index][i]._width - base._width) / 2;
				base.y = (this.game.items[index][i]._height - base._height) + 10;
				base.isZoomed = false;

				base.interactive = true;
				base.buttonMode = true;
				base.defaultCursor = "pointer";
				/**
				 * [Adding click and tap event to item]
				 * @return {[Object]} [on click add item to basket]
				 */
				base.tap = base.click = () => {
				    this.flushTimeOuts();
				    /**
                     * Add google analytics on buy Item click
                     */
                    if(new Device().isMobile()) {
                        ga('send', 'event', 'Bike Shop', 'touch_input', 'Purchase_Tap_Mobile');
                    } else {
                        ga('send', 'event', 'Bike Shop', 'mouse_input', 'Purchase_Click_Desktop');
                    }
                    /**
                     * Reset the returning item texture to origin image
                     */
                    for(let ib = 0;ib < this.basket.children.length && this.clickStatus == 1;ib++) {
                        this.basket.children[ib].texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + this.basketArr[this.basket.children[ib].mapIndex].img);
                    }
                    this.clickStatus = 0;

                    // to prevent double click
					base.interactive = false;
					setTimeout(() => {
						base.interactive = true;
						this.updateView();
					}, 1000);

					if(this.overLay[index].visible == false) {
                        // total required for the item
                        let totalCoinRequired = this.registerCount.count + it.price;
                        // Making that much coin activate
                        //Checking for enough coin
                        if(totalCoinRequired > this.topCoins.children.length) {
                            ga('send', 'event', 'Bike Shop', 'denied_purchase', 'CannotPurchaseItem');
                            sound.play(this.opt.baseMediaUrl + this.opt.notEnoughCoin, () => {
                                Anim.reIlluminateOverlay(this.overLay[index]);
                            });
                            return;
                        }
						base.isZoomed = true;
						this.selectedItemPrice = it.price;
						base.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + it.img_glow);
						base.position.y = base.position.y - 20;
                        base.position.x = base.position.x - 20;
						// Animation Code
						let tween_base = PIXI.tweenManager.createTween(base);
						tween_base.easing = PIXI.tween.Easing.outBounce();
						tween_base.time = 1000;
						tween_base.loop = false;
						tween_base.to({
					      scale: {x: base.scale.x * 1.4, y: base.scale.y * 1.4}
					    });
					    tween_base.on('end', () => tween_base.stop().clear());
						tween_base.start();
						/**
						 * { making all item visible }
						 */
						// Setting the row and column for selected item
						this.selectedItem.row = index;
						this.selectedItem.col = i;
						// Reset Overlay visibility
						// Resetting the overLay index before clicking on perticular item
						for( let j = 0;j < this.game.items.length; j += 1) {
							for( let k = 0; k < this.game.items[j].length; k += 1) {
								/*if(this.game.items[j][k].base.visible) {
									this.overLay[j].visible = false;
								}*/
								this.item_container.setChildIndex(this.game.items[j][k], this.game.items[j][k].initIndex);
							}
						}
						this.updateOverlay();
						/**
						 * disable interaction of item
						 */
						//base.interactive = false;
						// show overlay on click
						this.overLay[index].visible = true;

						this.item_container.setChildIndex(this.game.items[index][i], this.item_container.children.length - 1);
						for(let k = 0;k < this.topCoins.children.length;k += 1) {
                            this.topCoins.children[k].interactive = true;
                        }
						// Play sounds on clicking sounds
						this.playItemClickAudio(index, i, () => {
							sound.stop();
                            // Play current price audio
							sound.play(this.opt.baseMediaUrl + 'BS_5.0_12' + (this.opt.levels.group[this.currentLevel].game.items[index][i].price - 1) +'.wav', () => {
								let d = new Device();
								//play the tap/click audion
								if(d.isMobile()) {
									sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[index][i].payFor, () => {
                                        sound.play(this.opt.baseMediaUrl + 'BS_5.0_12' + (this.opt.levels.group[this.currentLevel].game.items[index][i].price - 1) +'.wav', () => {
                                            this.touchtimeinterval = setTimeout(() => {
                                                sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].payDelay[0]);
                                            }, 8000);
                                            this.timeouts.push(this.touchtimeinterval);
                                        });
                                    });
								} else {
									sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[index][i].payForDesktop, () => {
                                        sound.play(this.opt.baseMediaUrl + 'BS_5.0_12' + (this.opt.levels.group[this.currentLevel].game.items[index][i].price - 1) +'.wav', () => {
                                            this.touchtimeinterval = setTimeout(() => {
                                                sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].payDelay[1]);
                                            }, 8000);
                                            this.timeouts.push(this.touchtimeinterval);
                                        });
                                    });
								}

							});
						});
                        this.resetRegister();

					} else {
						// stop all the sounds
						try {
							sound.stop();
						} catch(e) {
							console.log(e);
						}
						base.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + it.img);
                        base.scale.x = base.scale.x;
						base.scale.y = base.scale.y;
						base.position.y = base.position.y + 20;
                        base.position.x = base.position.x + 20;
						this.overLay[index].visible = false;
						base.isZoomed = false;
						this.selectedItem = {};
						//this.game.items[index][i].priceTag.scale.x = this.game.items[index][i].priceTag.scale.x / 1.3;
						//this.game.items[index][i].priceTag.scale.y = this.game.items[index][i].priceTag.scale.y / 1.3;
						for(let k = 0;k < this.topCoins.children.length;k += 1) {
							this.topCoins.children[k].interactive = false;
						}
                        this.registerCounterVal = 0;
                        /*
						 * Update register after unselect
						 */
						this.updateRegister();
                        this.resetRegister();
					}
					// Resetting the zoom for all the items
					for( let j = 0;j < this.game.items.length;j += 1) {
						if( j != index) {
							for( let k = 0;k < this.game.items[j].length; k += 1) {
								//console.log(this.game.items[j][k].base.isZoomed);
								if (this.game.items[j][k].base.isZoomed) {
									this.game.items[j][k].base.scale.x = this.game.items[j][k].base.scale.x;
									this.game.items[j][k].base.scale.y = this.game.items[j][k].base.scale.y;
                                    this.game.items[j][k].base.position.x = this.game.items[j][k].base.position.x + 20;
                                    this.game.items[j][k].base.position.y = this.game.items[j][k].base.position.y + 20;
									this.game.items[j][k].base.isZoomed = false;
								}
							}
						}

					}
					this.updateView();
				};
				this.game.items[index][i].base = base;
				this.game.items[index][i].addChild(this.game.items[index][i].base);
				this.item_container.addChild(this.game.items[index][i]);

				//get initial index
				//console.log(this.item_container.getChildIndex(this.game.items[index][i]));
				this.game.items[index][i].initIndex = this.item_container.getChildIndex(this.game.items[index][i]);
			});
			// Adding item container to basket
			/**
			 * Basket each item container
			 */
			//this.basket.item[index] = new PIXI.Container();
			/**
			 * [Properties basket each item container properties]
			 * @type {[Object]}
			 */
			//this.basket.item[index].height = this.basket._height;
			//this.basket.item[index].width = this.basket._width / this.opt.levels.group[this.currentLevel].game.items.length;
			//this.basket.item[index].x = (index * this.basket.item[index]._width);
			//this.basket.item[index].y = this.opt.baseHeight / 1.17;
			/**
			 * Adding basket item container to basket
			 */
			//this.basket.addChild(this.basket.item[index]);

			/**
			 * Adding disabled overlay
			 */
			this.overLay[index] = new PIXI.Graphics();
			// Setting properties for overlay
			this.overLay[index].beginFill(0x000000, 0.7);

			let totalLength = this.opt.levels.group[this.currentLevel].game.items.length;

			this.overLay[index].drawRect(0 , (index * (this.item_container._height / totalLength)), this.item_container._width - (this.opt.baseWidth / 76.1), this.item_container._height / totalLength);

			this.overLay[index].interactive = true;
			this.overLay[index].visible = true;
			// preventing event propagation
			this.overLay[0].tap = this.overLay[0].click = (e) => {
			    /**
			    * If played the current level once
			    */
			    if(this.opt.levels.group[this.currentLevel].played) {
			        if((this.basketArr.length > 0) || (JSON.stringify(this.selectedItem) != '{}') ){
			            return;
                    }
			        sound.stop();
			        this.overLay[0].visible = false;
                    sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[index][0].chooseAudio, () => {
                        this.touchtimeinterval = setTimeout(() => {
                            if(new Device().isMobile()) {
                                sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[index][0].delayPlay[0]);
                            } else {
                                sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[index][0].delayPlay[1]);
                            }

                        }, 8000);
                        this.timeouts.push(this.touchtimeinterval);
                    });
			    }

				e.stopPropagation(true);
			}
			/*if(index == 0) {
				this.overLay[index].visible = false;
			}*/

			this.item_container.addChild(this.overLay[index]);
		});

		/**
		 * [coinContainer add register container]
		 * @type {Object}
		 */
		this.coinContainer = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.coinContainer);
		this.coinContainer.x = this.opt.baseWidth / 1.4096;
		this.coinContainer.y = this.opt.baseHeight / 1.4965;
		this.coinContainer.height = this.opt.baseHeight / 3.4375;
		this.coinContainer.width = this.coinContainer._height / (this.coinContainer.texture.height / this.coinContainer.texture.width);
		this.coinContainer.visible = false;
		this.game_stage.addChild(this.coinContainer);

		//this.game_stage.setChildIndex(this.game.fakeBg, this.game_stage.children.length - 1);
		this.game_stage.setChildIndex(this.coinContainer, this.game_stage.children.length - 1);
		/**
		 * Add coins to the top bar
		 */
		this.addTopCoins();
		/**
		 * Add register
		 */
		this.addRegisterCount();
		/**
		 * Add next arrow
		 */
		this.addNextArrow();

		this.container.addChild(this.game_stage);
		this.setHomeIconIndex();
		//this.climaxAnimation();

		// Do the coin animation if this level is not played
        let animateCoinTimeout = null;
        if(!this.opt.levels.group[this.currentLevel].played) {
            animateCoinTimeout = setTimeout(() => {
                Anim.animateCoins(this.topCoins, this.topCoinsGlow);
            }, 1500);
        }
		/**
		 * Sound play for Shop screen
		 */
		sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].intro.coinAudio, () => {
			//Play the register audio
			sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].intro.register, () => {
				let d = new Device();
				// Do register animation
				if(d.isMobile()) {
					sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].intro.tapMobile, () => {
						this.overLay[0].visible = false;
						sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[0][0].chooseAudio, () => {
                            this.touchtimeinterval = setTimeout(() => {
                                sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[0][0].delayPlay[0]);
                            }, 8000);
                            this.timeouts.push(this.touchtimeinterval);
                        });
					});
				} else {
					sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].intro.clickDesktop, () => {
						this.overLay[0].visible = false;
						sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[0][0].chooseAudio, () => {
                            this.touchtimeinterval = setTimeout(() => {
                                sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[0][0].delayPlay[1]);
                            }, 8000);
                            this.timeouts.push(this.touchtimeinterval);
                        });
					});
				}

			});
		});

		this.updateView();
	}
	/**
	 * [updateOverlay show and hide the respective overlay]
	 * @param  {[Number]} idx [index of overlay]
	 * @return {[Object]}     [context]
	 */
	updateOverlay() {
		/**
		 * [if index is not equal to overlay length]
		 * @param  {[Number]} idx !             [index of overlay]
		 * @return {[Object]}     [description]
		 */
		let status = false;
		for(let i = 0;i < this.game.items.length;i += 1) {
			for(let k = 0; k < this.game.items[i].length; k += 1 ) {
				if( this.game.items[i][k].visible == false ) {
					this.overLay[i].visible = true;
                    break;
				} else {
					this.overLay[i].visible = false;
				}
            }
			if(this.overLay[i].visible == false) {
				break;
			}
		}

		this.updateView();

	}
	/**
	 * [addBasketPriceTag pricetag for basket]
	 */
	addBasketPriceTag() {
		/**
		 * [basket_price basket price array]
		 * @type {Array}
		 */
		this.basket_price = [];
		// Looping over the baskets
		for(let k = 0, l = this.basket.children.length;k < l; k += 1) {
			/**
			 * creating new container for basket container
			 */
			//console.log(this.basket.position.x, this.basket._width);
			this.basket_price[k] = new PIXI.Container();
			this.basket_price[k].visible = false;
			this.basket_price[k].width = Math.ceil(this.basket._width) / l;
			//console.log(this.opt.baseWidth / 6.4646);
			this.basket_price[k].x = this.basket.position.x + (this.basket_price[k]._width * k);
			//console.log(this.basket_price[k].position.x);
			/**
			 * { image tag for basket price tag }
			 */
			this.basket_price[k].tag = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].game.priceTag);
			/**
			 * [Properties properties for basket price]
			 * @type {[Object]}
			 */
			//console.log(this.basket.position.x , this.basket.item[k].position.x);
			this.basket_price[k].tag.x = this.basket_price[k]._width  / 4;
			this.basket_price[k].tag.y = -10;
			/*this.basket_price[k].tag.scale.x = 0.7;
			this.basket_price[k].tag.scale.y = 0.7;*/
			/**
			 * [price basket pricetag text]
			 * @type {Object}
			 */
			this.basket_price[k].price = new PIXI.Text('0' ,{font : '40px Arial', fill : 0xBC6428, align : 'center'});
			/**
			 * [Properties basket price tag properties]
			 * @type {Object}
			 */
			this.basket_price[k].price.rotation = -45;
			this.basket_price[k].price.x = this.basket_price[k].tag.position.x + (this.basket_price[k].tag.texture.width / 4);
			this.basket_price[k].price.y = this.basket_price[k].tag.position.y + (this.basket_price[k].tag.texture.height / 2);

			/**
			 * Add basket price tag tp the fake bg
			 */
			this.basket_price[k].addChild(this.basket_price[k].tag);
			this.basket_price[k].addChild(this.basket_price[k].price);
			//this.game.fakeBg.addChild(this.basket_price[k]);
			//this.game.fakeBg.setChildIndex(this.basket_price[k], this.game.fakeBg.children.length - 1);
		}
	}
	addToBasket(child, index, i) {
		this.clickStatus = 0;
        //console.log(this.basketArr);
		let item_select = this.opt.levels.group[this.currentLevel].game.items[index][i];
		this.basketArr[item_select.priority] = item_select;
        this.flushTimeOuts();

        /**
         * Add timeinterval if user is not clicking any item for 8 seconds
         */
        if(index < this.basketArr.length - 1) {
            //console.log(index);
            this.touchtimeinterval = setTimeout(() => {
                if(new Device().isMobile()) {
                    sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[index + 1][0].delayPlay[0]);
                } else {
                    sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[index + 1][0].delayPlay[1]);
                }
            }, 8000);
            this.timeouts.push(this.touchtimeinterval);
        }


		//console.log(this.basketArr);
		/**
		 * [item item image]
		 * @type {[Object]}
		 */
		let item = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + item_select.img);
		/**
		 * [Properties item properties]
		 * @type {[Object]}
		 */
		item.width = this.opt.baseWidth / 5.8333;
        /**
         * Only for the grover level
         */
        if(this.opt.levels.group[this.currentLevel].name == 'grover') {
            item.width = this.opt.baseWidth / 7.0333;
        }
		item.height = item._width * (item.texture.height / item.texture.width);
		item.x = index * (this.basket._width / 3);
		//console.log(this.basket._height - item.height);
		item.y = this.basket._height - item._height;
		/*item.scale.x = item.scale.x * 1.9;
		item.scale.y = item.scale.y * 1.9;*/
		item.interactive = true;
		item.buttonMode = true;
		item.defaultCursor = "pointer";
		item.mapIndex = item_select.priority;

        /**
		 * [Drag]
		 * @return {[Void]} [description]
		 */
		let clickOnItem = (event) => {
			sound.stop();
            this.flushTimeOuts();
			// On first click check the clickStatus , if 0 then play audio to click again
			if(this.clickStatus == 0) {
                /*
                 * hide the glow coin and show the normal accounts
                 */
                if(this.basketArr.length == 3) {
                    this.topCoins.visible = true;
                    this.topCoinsGlow.visible = false;
                }
                this.clickStatus++;
				let d = new Device();
				// Checking for device
				if(d.isMobile()) {
					sound.play(this.opt.baseMediaUrl + this.opt.differentChoiceMobile);
				} else {
					sound.play(this.opt.baseMediaUrl + this.opt.differentChoiceDesktop);
				}
				if(JSON.stringify(this.selectedItem) == '{}') {
                    this.selectedItem.row = index;
                    this.selectedItem.col = i;
                }
				let clickedItem = this.game.items[this.selectedItem.row][this.selectedItem.col].base;
				clickedItem.scale.x = clickedItem.scale.x;
				clickedItem.scale.y = clickedItem.scale.y;
                clickedItem.position.x = clickedItem.position.x + 20;
                clickedItem.position.y = clickedItem.position.y + 20;
                clickedItem.isZoomed = false;
                /*if(this.selectedItem.row != index) {
                    this.overLay[index].visible = false;
                }*/
                /*
                 * Disable other items from returning
                 */
                for( let tp = 0;tp < this.basketArr.length;tp++) {
                    if((tp != index) && (this.basket.children[tp])) {
                        this.basket.children[tp].interactive = false;
                    }
                }

                this.item_container.setChildIndex(this.game.items[this.selectedItem.row][this.selectedItem.col], this.game.items[this.selectedItem.row][this.selectedItem.col].initIndex);
				clickedItem.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].game.items[this.selectedItem.row][this.selectedItem.col].img);
				item.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + item_select.img_glow);

				this.updateView();
			}
			// if clickStatus == 1 the move the item to the shelf
			else if(this.clickStatus == 1) {
                /**
                 * Add google analytics on item return
                 */

                ga('send', 'event', 'Bike Shop', 'item_returned', 'ItemReturned');

				//make the items on shelf visible
				this.game.items[index][i].base.scale.x = this.game.items[index][i].base.scale.x;
				this.game.items[index][i].base.scale.y = this.game.items[index][i].base.scale.y;
                this.game.items[index][i].base.position.x = this.game.items[index][i].base.position.x + 20;
                this.game.items[index][i].base.position.y = this.game.items[index][i].base.position.y + 20;
				this.game.items[index][i].base.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].game.items[index][i].img);
				// Show the overlay
				//this.overLay[index].visible = false;
				this.game.items[index][i].base.interactive = true;
				// remove the item from the basket
                let returnItemPrice = this.basketArr[this.opt.levels.group[this.currentLevel].game.items[index][i].priority].price;
                this.basketArr[this.opt.levels.group[this.currentLevel].game.items[index][i].priority] = null;
                //this.basketArr.splice(this.basketArr.indexOf(returnItem), 1);
				//console.log(this.basketArr);
				// Remove child from basket
				//let itemGlobalPos = item.parent.toGlobal(item.parent.position);

				let initItemGlobalPos = JSON.stringify(item.parent.position);

                sound.play(this.opt.baseMediaUrl + 'coin_return.mp3');
				Anim.pathAnimation(this.opt, this.container, { x : ((this.opt.baseWidth / 2) - 100), y : this.topCoins.position.y} , 1000, returnItemPrice, () => {
					item.parent.position = JSON.parse(initItemGlobalPos);
					this.game.items[index][i].visible = true;
					this.basket.removeChild(item);
                    this.overLay.map((over) => {
                        over.visible = true;
                    });
                    this.overLay[index].visible = false;
                    /*
                     * Enable other items from returning
                     */
                    //console.log(this.basketArr.length, this.basket.item);
                    for( let tp = 0;tp < this.basket.children.length;tp++) {
                        if(this.basket.children[tp]) {
                            this.basket.children[tp].interactive = true;
                        }
                    }
                    if(new Device().isMobile()) {
                        sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[index][i].returnItem);
                    } else {
                        sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[index][i].returnItemDesktop);
                    }

                    this.resetRegister();
                    this.updateView();
                });
                /**
                 * Update the texture for the returned item
                 * @type {*}
                 */
                //item.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + item_select.img);
				// Update register
				this.updateRegister();
				// Make the clickStatus = 0 , resetting the clickStatus
                this.clickStatus = 0;

			}
        }

		//Bind events to the item (mousedown, mousemove, mouseup)
		item.on('click', clickOnItem);
		item.on('tap', clickOnItem);

		//this.basket.removeChildren(0, this.basket.children.length);
		/**
		 * { Add child to respective basket }
		 */
		this.basket.addChild(item);
		/**
		 * Updating the view
		 */
		this.updateView();
	}
	/**
	 * [registerCount when coin added the counter will increase]
	 * @return {[Object]} [description]
	 */
	addRegisterCount() {
		/**
		 * [registerCount text count]
		 * @type {Object}
		 */
		let font = (this.opt.baseHeight / 23.7812) + 'px Arial';
		this.registerCount = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].registerCount + '0.png');
		//console.log(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].registerCount + '0.png');
		// registerCount properties
		this.registerCount.height = this.opt.baseHeight / 16.3684;
		this.registerCount.width = this.registerCount._height / (this.registerCount.texture.height / this.registerCount.texture.width);
		this.registerCount.x = (this.opt.baseWidth / 1.105) - this.registerCount._width;
		this.registerCount.y = this.opt.baseHeight / 1.355;
		this.registerCount.count = 0;
		this.registerCount.visible = false;
		//console.log(this.registerCount);
		this.game_stage.addChild(this.registerCount);

		/**
		 * Show coin in the register
		 */
		//this.regCoin = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].registerCoin);
		//regCoin properties
		//this.regCoin.x = this.opt.baseWidth / 1.3368;
		//this.regCoin.y = this.opt.baseHeight / 1.48;
		//this.regCoin.width = this.opt.baseWidth / 16.57;
		//this.regCoin.height = this.regCoin._width * (this.regCoin.texture.height / this.regCoin.texture.width);
		/*this.regCoin.scale.x = 0.4;
		this.regCoin.scale.y = 0.4;*/
		//this.regCoin.visible = false;
		//this.game_stage.addChild(this.regCoin);

		/**
		 * Update the view
		 */
		this.updateView();
		//this.animateRegisterBox();

    }
	/**
	 * [updateRegister update the register the count]
	 * @return {[Object]} [description]
	 */
	updateRegister() {
		// check if all items are added to basket
		let goCount = 0;
		//Checking if all basket has children
		let count = 0;
		console.log(this.basketArr);
		for(let c = 0; c < this.basketArr.length; c += 1) {
			if(this.basketArr[c]) {
				count = count + this.basketArr[c].price;
				goCount++;
			}
		}

		/*if(count == 0) {
			this.registerCount.visible = false;
		} else {
			this.registerCount.visible = true;
		}*/
		/**
		 * Update coin count
		 */
		//make all visible
		for(let mv = 0; mv < this.topCoins.children.length; mv += 1) {
			this.topCoins.children[mv].visible = true;
			this.topCoins.children[mv].interactive = false;
		}
		// make visible false same as count
		console.log(count);
		for(let c = 0;c < count;c += 1) {
			this.topCoins.children[c].visible = false;
		}
		/**
		 * [if toggle visible]
		 */
		/*if(count == 0) {
			this.regCoin.visible = false;
		} else {
			this.regCoin.visible = true;
		}*/
		this.registerCount.count = count;
		/*this.registerCount.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].registerCount + count + '.png');*/

		//console.log(goCount);
		/**
		 * { Toggle next arrow }
		 */
		//console.log(this.opt.levels.group[this.currentLevel].game.items.length);
		//console.log("go Count", goCount);
        /*
         * Hide the skip button
         */
        this.nextArrow.visible = false;

		if(this.opt.levels.group[this.currentLevel].game.items.length == goCount) {
            this.topCoins.visible = false;
            this.topCoinsGlow.visible = true;
            /*
             * Show the skip button
             */
            this.nextArrow.visible = true;
            this.topCoinsGlow.children.map((gcoin, index) => {
                if(index < count) {
                    gcoin.visible = false;
                }
            });
			window.setTimeout(() => {
			    this.basket.children.map((itm, idx) => {
                    this.basket.children[idx].interactive = true;
                });
                this.updateView();
                //this.elmo.visible = false;
                //this.game_stage.removeChildren(0, this.game_stage.children.length);
                try {
                    sound.stop();
                } catch(e) {
                    console.log(e);
                }
                let fixAudioPlay = () => {
                    sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].totalProduct.fixAudio, () => {
                        this.game_stage.removeChildren(0, this.game_stage.children.length);
                        //this.game_stage = new PIXI.container();
                        this.createProduct();
                        this.updateView();
                        // Loading other assets
                        this.loadAssetsForBikeBuilding();

                        window.removeEventListener('resize', this.resizeGame);
                        this.shopResize = false;
                        this.buildResize = true;
                        window.addEventListener('resize', this.resizeBuildScreen.bind(this), false);
                    });
                }
                var cnt = (this.topCoins.children.length - this.registerCount.count - 1);
                /**
                 * If no coin saved then play no coin saved audio
                 */
                if(cnt < 0) {
                    console.log('Hello world');
                    sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].totalProduct.noSaveAudio, () => {
                        fixAudioPlay();
                    });
                } else {
                    /**
                     * Add google analytics on saved coins
                     */
                    ga('send', 'event', 'Bike Shop', 'coins_saved', this.opt.levels.group[this.currentLevel].name + '_' + cnt);
                    sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].totalProduct.boughtCompleteAudio, () => {
                        sound.play(this.opt.baseMediaUrl + 'BS_5.0_12' + cnt + '.wav', () => {
                            fixAudioPlay();
                        });
                    });
                }
            }, 500);
		}
		/**
		 * Update the view
		 */
		this.updateView();
	}
	/**
	 * Adds a next arrow.
	 */
	addNextArrow() {
		/**
		 * [nextArrow new image container]
		 * @type {[Object]}
		 */
		this.nextArrow = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.nextArrow);
		//nextArrow properties
		this.nextArrow.x = this.opt.baseWidth - (this.opt.baseWidth / 9.9781);
		this.nextArrow.y = this.opt.baseHeight / 2.6341 + 15;
		this.nextArrow.height = this.opt.baseHeight / 7.5;
		this.nextArrow.width = this.nextArrow._height / (this.nextArrow.texture.height / this.nextArrow.texture.width);
		this.nextArrow.visible = false;

		let next_tween = PIXI.tweenManager.createTween(this.nextArrow);
		next_tween.to({
	      scale: {
	      	x: this.nextArrow.scale.x * 1.1,
	      	y: this.nextArrow.scale.y * 1.1
	      }
	    });
	    next_tween.loop = true;
	    next_tween.time = 500;
	    next_tween.start();
		// Adding click event
		this.nextArrow.interactive = true;
		this.nextArrow.tap = this.nextArrow.click = () => {
			this.game_stage.removeChildren(0, this.game_stage.children.length);
            this.createProduct();
			this.updateView();
			var loader = new Loader('loading');
			loader.show();
            this.loadAssetsForBikeBuilding();
			//this.playItemAudio(this.opt.levels.group[this.currentLevel].totalProduct.helpAudio);

            window.removeEventListener('resize', this.resizeGame);
            this.shopResize = false;
            this.buildResize = true;
            window.addEventListener('resize', this.resizeBuildScreen.bind(this), false);
		}
		// Add next arrow to game stage
		this.game_stage.addChild(this.nextArrow);
		/**
		 * Update view
		 */
		this.updateView();
	}
	/**
	 * Creates a product.
	 * Assemble all the products and create cycle
	 */
	createProduct() {
        this.flushTimeOuts();
	    /**
		 * [boughtItems the items which user bought]
		 * @type {Array}
		 */
		this.boughtItems = [];
		this.delayTimeoutForDrag = null;
		let currentLevelName = this.opt.levels.group[this.currentLevel].name;
        // stop all instance of sounds
        sound.stop();
		/**
		 * [cyclebg cycle background image]
		 * @type {[Object]}
		 */
		let cyclebg = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].totalProduct.cyclebg);
		// Properties for cyclebg
		cyclebg.width = this.opt.baseWidth;
		cyclebg.height = this.opt.baseHeight;
		cyclebg.y = 0;
		cyclebg.x = 0;
		/**
		 * [cycle cycle image]
		 * @type {[Object]}
		 */
		let cycle = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].totalProduct.cycle);
		// Properties for cycle
		if(currentLevelName == 'cookie') {
			cycle.height = this.opt.baseHeight / 1.8882;
			cycle.y = this.opt.baseHeight / 2.6857;
			cycle.x = this.opt.baseWidth / 2.432;
		}
		if(currentLevelName == 'elmo') {
			if(this.basketArr[2].alias == 'Blw') {
				cycle = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].totalProduct.cycle);
			} else if(this.basketArr[2].alias == 'Brw') {
				cycle = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].totalProduct.cycle_alt);
			}

			cycle.height = this.opt.baseHeight / 2.4882;
			cycle.y = this.opt.baseHeight / 1.8857;
			cycle.x = this.opt.baseWidth / 1.932;
		}
		if(currentLevelName == 'grover') {
			cycle.height = this.opt.baseHeight / 2.4882;
			cycle.y = this.opt.baseHeight / 1.8857;
			cycle.x = this.opt.baseWidth / 1.732;
		}

		cycle.width = cycle._height / (cycle.texture.height / cycle.texture.width);

		/**
		 * [drop_zone Drag and Drop selected item here]
		 * @type {PIXI}
		 */
		let drop_zone = new PIXI.Container();
		drop_zone.width = this.opt.baseWidth / 2.24561;
		drop_zone.height = this.opt.baseHeight / 1.0443;
		drop_zone.x = this.opt.baseWidth / 3.1411;
		drop_zone.y = 0;
		/**
		 * Adding all blank containers to the parent container
		 */
		this.game_stage.addChild(cyclebg);
		this.game_stage.addChild(cycle);
		this.game_stage.addChild(drop_zone);

		let bought_item_height = Math.floor(this.opt.baseHeight / 1.2222) / this.basketArr.length;
		/**
		 * [creating and Adding all elements to the container]
		 * @param  {[Array]} (itm, idx           [description]
		 * @return {[Object]}       [description]
		 */
		this.basketArr.map((itm, idx) => {
			//console.log(itm);
			let bought = null;
			bought = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + itm.bought_img);
			bought.width = this.opt.baseWidth / 8.0196;
			bought.height = bought._width * (bought.texture.height / bought.texture.width);
			bought.y = (this.opt.baseHeight / 8.8) + (idx * bought_item_height) + ((bought_item_height - bought._height) / 2);
			bought.x = this.opt.baseWidth / 29.0909;
			bought.interactive = true;
			bought.buttonMode = true;
			bought.defaultCursor = "pointer";
			let isDragging = false;
			let data = null;
			let initialPos = JSON.stringify({x : bought.position.x, y : bought.position.y});
			this.draggedItemCount = 0;
			/**
			 * [dragStart start draggind]
			 * @return {[Void]} [description]
			 */
			let dragStart = (event) => {
			    sound.play(this.opt.baseMediaUrl + this.opt.click_sound);
				//console.log(item);
				data = event.data;
				isDragging = true;
				// Clear the delay timeout audio
				this.delayTimeoutForDrag = null;
			}
			/**
			 * [dragMove on drag move]
			 * @return {[Void]} [description]
			 */
			let dragMove = () => {
				if(isDragging) {
					var newPosition = data.getLocalPosition(bought.parent);
					bought.position.x = newPosition.x - (bought._width / 2);
			        bought.position.y = newPosition.y - (bought._height / 2);
			        this.updateView();
				}
			}
			/**
			 * [dragEnd on drag end]
			 * @return {[Void]} [description]
			 */
			let dragEnd = () => {
				data = null;
				isDragging = false;
				// Clear the delay timeout audio
				this.delayTimeoutForDrag = null;
				fitItemToProduct(bought, idx);
				this.updateView();
			}
			/**
			 * [Fitting the bought products to the cycle]
			 * @param  {[Object]} basket [parent basket]
			 * @param  {[Object]} buy    [each bought item]
			 * @return {[Object]}        [description]
			 */
			let fitItemToProduct = (buy, index) => {
                this.flushTimeOuts();
                if(new Device().isMobile()) {
                    this.touchtimeinterval = setTimeout(() => {
                        sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].fixDelay[0]);
                    }, 8000);
                } else {
                    this.touchtimeinterval = setTimeout(() => {
                        sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].fixDelay[1]);
                    }, 8000);
                }
                this.timeouts.push(this.touchtimeinterval);
				//console.log(buy, index);
				/**
				 * [rangeX calculating the range in x axis for the dropping item ]
				 * @type {Object}
				 */
				//let rangeX = {};
				//rangeX.x1 = Math.floor(drop_zone.position.x);
				//rangeX.x2 = Math.floor(drop_zone.position.x + drop_zone._width);
				/**
				 * [rangeY calculating the range in y axis for the dropping item]
				 * @type {Object}
				 */
				//let rangeY = {};
				//rangeY.y1 = Math.floor(drop_zone.position.y);
				//rangeY.y2 = Math.floor(drop_zone.position.y + drop_zone._height);
				//console.log(buy.bgImage.scale.x, buy.bgImage.scale.y, buy.itemImage.scale.x, buy.itemImage.scale.y);
				/**
				 * [itemPosX calculating the global position for the item dragged in x axis]
				 * @type {[Number]}
				 */
				//let itemPosX = Math.floor(buy.position.x);
				/**
				 * [itemPosY calculating the global position for the item dragged in y axis]
				 * @type {[Number]}
				 */
				//let itemPosY = Math.floor(buy.position.y);
				//if((itemPosX < rangeX.x2 && itemPosX > rangeX.x1) && (itemPosY < rangeY.y2 && itemPosY > rangeY.y1)) {
					buy.interactive = false;
					buy.visible = false;
					let combined_img = '';
					/**
					 * [clevel Object for the current level]
					 * @type {[Object]}
					 */
					let clevel = this.opt.levels.group[this.currentLevel];
					//if the level has head drop
					if(this.basketArr[index].head) {
						let headImg = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + clevel.name + '/' + clevel.totalProduct.headAlias + '_' + this.basketArr[index].alias + '.png');
						/**
						 * For cookie level setting different property
						 * @type {[Object]}
						 */
						if(clevel.name == 'cookie') {
							headImg.width = this.opt.baseWidth / 4.41379;
							headImg.height = headImg.width * (headImg.texture.height / headImg.texture.width);
							//this.opt.baseHeight / 2.5641;

							headImg.x = this.opt.baseWidth / 2.46746;
							headImg.y = (this.opt.baseHeight / 2.06185) - headImg._height;
						}
						/**
						 * For Grover level setting different property
						 * @type {[Object]}
						 */
						if(clevel.name == 'grover') {
							headImg.width = this.opt.baseWidth / 5.14572;
							headImg.height = headImg.width * (headImg.texture.height / headImg.texture.width);
                                ////this.opt.baseHeight / 2.57777;

							headImg.x = this.opt.baseWidth / 2.82872;
							headImg.y = (this.opt.baseHeight / 1.68539) - headImg._height;
						}


						this.game_stage.addChild(headImg);
						//this.draggedItemCount = this.draggedItemCount + 1;
					} else {
						/**
						 * if drgaed item empty push the item of splice the item
						 */
						this.draggedItem[this.basketArr[index].priority] = this.basketArr[index].alias;
						for(let i = 0;i < this.draggedItem.length;i += 1) {
							if(!this.draggedItem[i]) {
								continue;
							} else {
								let dg = '_' + this.draggedItem[i];
								combined_img = combined_img + dg;
							}
						}
						//Setting new combined image after drag
						if(this.boughtItems[2].visible == true && currentLevelName == 'elmo') {
						    cycle.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + clevel.name + '/' +  this.basketArr[2].alias + '_' + clevel.totalProduct.alias + combined_img + '.png');
						} else {
						    cycle.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + clevel.name + '/' +  clevel.totalProduct.alias + combined_img + '.png');
						}
						//console.log(this.opt.baseImageUrl + clevel.name + '/' +  clevel.totalProduct.alias + combined_img + '.png');

					}
					this.draggedItemCount = this.draggedItemCount + 1;
					//this
					// if dragged item length == bought items length then go to the next screen
					if(this.draggedItemCount == this.basketArr.length) {
						//buy.bgImage.visible = false;
						//After fixing the cycle play the complete audio
                        sound.stop();
						sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].totalProduct.dragCompleteAudio, () => {
							//console.log('teststststs');
                            this.buildResize = false;
                            this.payoffResize = true;
                            window.removeEventListener('resize', this.resizeBuildScreen);
                            window.addEventListener('resize', this.resizePayoffScreen.bind(this), false);
							this.climaxAnimation();
						});
					}
					this.updateView();
				//}
				//else {
					/**
					 * [x resetting the x position if not dropped in range x]
					 * @type {[Number]}
					 */
				//	buy.x = JSON.parse(initialPos).x;
					/**
					 * [y resetting the x position if not dropped in range y]
					 * @type {[Number]}
					 */
				//	buy.y = JSON.parse(initialPos).y;
					//console.log(initialPos);
				//}
			}
			/**
			 * Adding touch and drag events to the bought elements
			 */
			/*
			bought.on('touchstart', dragStart);
			bought.on('mousedown', dragStart);

			bought.on('touchmove', dragMove);
			bought.on('mousemove', dragMove);

			bought.on('touchend', dragEnd);
			bought.on('mouseup', dragEnd);

			bought.on('mouseupoutside', dragEnd)
			bought.on('touchendoutside', dragEnd);*/

            /**
             * Add Click event to bought item
             */
            let clickOnBoughtItem = () => {
                /**
                 * Add google analytics on Building screen
                 */
                if(new Device().isMobile()) {
                    ga('send', 'event', 'Bike Shop', 'touch_input', 'Vehicle_Tap_Mobile');
                } else {
                    ga('send', 'event', 'Bike Shop', 'mouse_input', 'Vehicle_Click_Desktop');
                }


                sound.play(this.opt.baseMediaUrl + this.opt.click_sound);
                // Sound instance is there then remove it
                if(this.delayTimeoutForDrag) {
                    window.clearTimeout(this.delayTimeoutForDrag);
                    this.delayTimeoutForDrag = null;
                }

                this.flushTimeOuts();
                 /**
                 * Checking if helmet is present in the bought item
                 */
                if(this.basketArr[idx].head) {
                    var to = {
                        x: this.opt.baseWidth / 2.82872,
                        y :this.opt.baseHeight / 2.5873
                    };
                    Anim.moveAnimation(bought, to, 1000, () => {
                        fitItemToProduct(bought, idx);
                    });
                } else {
                    var to = {
                        x: this.opt.baseWidth / 1.61771,
                        y :this.opt.baseHeight / 1.65832
                    };
                    Anim.moveAnimation(bought, to, 1000, () => {
                        fitItemToProduct(bought, idx);
                    });
                }

            }

            bought.on('click', clickOnBoughtItem);
            bought.on('tap', clickOnBoughtItem);
			/**
			 * Updating the view
			 */
			this.game_stage.addChild(bought);
			this.updateView();
			this.boughtItems[idx] = bought;

			//Sounds for dragging items to the cycle
			try {
				sound.stop();
			} catch(e) {
				console.log(e);
			}
			if(new Device().isMobile()) {
                sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].totalProduct.dragAudio);
            } else {
                sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].totalProduct.dragAudioDesktop);
            }
            // Play the delay audio if the user is not dragging the item from the list
			/*this.delayTimeoutForDrag = setTimeout(() => {
                if(new Device().isMobile()) {
                    sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].totalProduct.delayAudio);
                } else {
                    sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].totalProduct.delayAudioDesktop);
                }
			}, 14000);*/
		});

		this.updateView();
	}

	climaxAnimation() {
	    this.flushTimeOuts();
		this.game_stage.removeChildren(0, this.game_stage.children.length);
        let game_config = window.app_setting;// JSON.parse(localStorage.getItem('elmo'));
		if(game_config.levels.group.length > (this.currentLevel + 1)) {
			game_config.levels.group[this.currentLevel + 1].locked = false;
			game_config.levels.group[this.currentLevel].played = true;
			window.app_setting = game_config;
			//localStorage.setItem('elmo', new Stringify(game_config).toString());
		}

		/*let climaxBg = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].climaxBg);
		climaxBg.x = 0;
		climaxBg.y = 0;
		climaxBg.width = this.opt.baseWidth;
		climaxBg.height = this.opt.baseHeight;
		this.game_stage.addChild(climaxBg);*/
		//console.log(seqImgArr);
		let animateOption = {
			imgArr : this.animArr,
			height: (this.opt.baseHeight / 1.5),
			ratio: 0.8,
			baseUrl: this.opt.baseImageUrl,
			x: (this.opt.baseWidth / 3.2),
			y: (this.opt.baseHeight / 3.4),
			renderer: this.renderer,
			container: this.container,
			containerHeight: this.opt.baseHeight,
			containerWidth: this.opt.baseWidth,
			background: this.opt.levels.group[this.currentLevel].climaxBg
		}
		this.animate = new Animate(animateOption);
		this.game_stage.addChild(this.animate.animate_container);
		this.animate.bgMoveAnimation();
		this.animate.climaxAnimate();
		//this.payOffScreenSkipButton();
        /**
         *
         */

		this.updateView();
		sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].payoff, () => {
            //stop the sounds and release them
            sound.stop();
            //stop animation
            this.animate.cancelAnimate();
            //stop all frames
            this.stopFrame();
            //Destroy the container
            this.container.destroy();
            //update localStorage
            //localStorage.setItem('elmo', new Stringify(game_config).toString());
            window.app_setting = game_config;
            //get localStorage data and update the options
            this.opt = window.app_setting;
            //create new levels
            let levels = new Levels(new PIXI.Container(), this.opt, this.renderer);
            levels.addBackground();
            levels.addLevels();
            //Destroy all Objects
            this.animate.destroy();
            this.destroy();
            // Play next level audio
            levels.playLevelAudio();
        });
		/*this.animationTimeout = setTimeout(() => {

		}, 10000);*/
	}
	/**
	 * [updateView refresh the container]
	 * @return {[Object]} [description]
	 */
	updateView() {
		this.renderer.render(this.container);
	}
	/**
	 * { Play list of audio }
	 *
	 * @param      {<Array>}    arr  list of audios
	 * @param      {Function}  callback  after all the audio play done
	 */
	playItemAudio(arr, callback) {
		try {
			sound.stop();
		} catch(e) {
			console.log(e);
		}
		let count = 0;
		let playAudio = () => {
			sound.play(this.opt.baseMediaUrl + arr[count], () => {
				count ++ ;
				if(arr.length > count) {
					return playAudio();
				} else {
					if(callback) {
						callback();
					}
					return false;
				}
			});
		}
		playAudio();
	}
	/**
	 * { Play audio on click of purchase item }
	 *
	 * @param      {<Number>}    index     array[index]
	 * @param      {<Number>}    i         { array[index][i] }
	 * @param      {Function}  callback  after the audio play
	 */
	playItemClickAudio(index, i , callback) {
		try {
			sound.stop();
		} catch(e) {
			console.log(e);
		}
		sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].game.items[index][i].clickAudio, () => {
			if(callback) {
				callback();
			}
		});
	}
	/**
	 * Reset the register counter
	 */
    resetRegister() {
        this.registerCount.visible = false;
        this.registerCount.texture = PIXI.Texture.fromImage(this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].registerCount + '0.png');
    }

	frameAnimate(){
	  let requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
      this.frameAnim = requestAnimationFrame(() => {
      	this.frameAnimate();
      });
      this.renderer.render(this.container);
      PIXI.tweenManager.update();
    }

    /**
     * Load assets for bike building screen
     */
    loadAssetsForBikeBuilding() {
        var assetLoader = new PIXI.loaders.Loader();
        let path = '';
        for(var p = 0; p < this.basketArr.length; p += 1) {
            if(this.basketArr[p]) {
                path = path + (this.basketArr[p].alias + ((p == this.basketArr.length - 1) ? '' : '_'));
            }

        }
        // Combined path
        let r_path = this.opt.baseImageUrl + this.opt.levels.group[this.currentLevel].name + '/' + path + '.json';
        try{
            assetLoader.add(r_path);
        }
        catch(e) {
            console.log(e);
        }

        assetLoader.once('complete',(d) => {
            var loader = new Loader('loading');
            loader.hide();
            this.animArr = d.resources[r_path].data.frames;
        });
        assetLoader.load();
    }

    /**
     * Payoff screen skip button
     */
    payOffScreenSkipButton() {
        let payoff_skip_button = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + 'skip.png');
        if(new Device().isMobile()) {
            payoff_skip_button.height = this.opt.baseHeight / 9.358;
            payoff_skip_button.x = this.opt.baseWidth - 40;
        } else {
            payoff_skip_button.height = this.opt.baseHeight / 11.358;
            payoff_skip_button.x = this.opt.baseWidth - 60;
        }
        payoff_skip_button.width = payoff_skip_button._height / (payoff_skip_button.texture.height / payoff_skip_button.texture.width);
        payoff_skip_button.y = this.opt.baseHeight / 2;
        payoff_skip_button.interactive = true;
        payoff_skip_button.buttonMode = true;
        payoff_skip_button.visible = false;
        payoff_skip_button.defaultCursor = "pointer";
        payoff_skip_button.tap = payoff_skip_button.click = () => {
            this.homeButtonClickAction();
        }
        this.game_stage.addChild(payoff_skip_button);

    }

    /**
     * Home button / skip button click action
     */
    homeButtonClickAction() {
        sound.stop();
        //sound.play(this.opt.baseMediaUrl + this.opt.click_sound);
        if(this.animate) {
            this.animate.cancelAnimate();
            this.animate.destroy();
        }
        this.stopFrame();
        this.container.destroy();
        /*if(this.animationTimeout) {
            window.clearTimeout(this.animationTimeout);
        }*/

        // get from localstorage
        //this.opt = JSON.parse(localStorage.getItem('elmo'));

        //get from window object
        this.opt = window.app_setting;

        //console.log(this.opt);
        let levels = new Levels(new PIXI.Container(), this.opt, this.renderer);
        levels.addBackground();
        levels.addLevels();
        levels.playLevelAudio();

        //Destroy all Objects
        this.destroy();

    }
    stopFrame(){
      window.cancelAnimationFrame(this.frameAnim);
    }
	/**
	 * Destroy all the Object de-allocate
	 */
	destroy() {
		this.opt = null;
		this.container = null;
		this.renderer = null;
		this.currentLevel = null;
		this.homeIcon = '';
		this.play = null;
		this.game_stage = null;
		this.game = {};
		this.topCoins = null;
		this.overLay = [];
		this.basketArr = [];
		this.selectedItem = {};
		this.animate = null;
		this.frameAnim = null;
		this.draggedItem = [];
		this.animArr = 0;
		for( let t = 0; t < this.timeouts.length; t += 1) {
			window.clearTimeout(this.timeouts[t]);
		}
	}

	addBackground() {
	    if(this.intro_bg) {
            this.container.removeChild(this.intro_bg);
        }
        /**
         * Adding background to the intro level background
         */
        this.intro_bg = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.intro.background);
        // Setting property for intro background
        this.intro_bg.height = this.opt.baseHeight;
        this.intro_bg.width = this.opt.baseWidth;
        this.container.addChild(this.intro_bg);
        // Lower index to background//
        this.container.setChildIndex(this.intro_bg, 0);
        this.updateView();

    }

    addSkipButton() {
        //Adding skip button
        // On click of this button the intro will skip
        if(this.skip_button) {
            this.container.removeChild(this.skip_button);
        }
        this.skip_button = new PIXI.Sprite.fromImage(this.opt.baseImageUrl + 'skip.png');
        if(new Device().isMobile()) {
            this.skip_button.height = this.opt.baseHeight / 9.358;
            this.skip_button.x = this.opt.baseWidth - 50;
        } else {
            this.skip_button.height = this.opt.baseHeight / 11.358;
            this.skip_button.x = this.opt.baseWidth - 100;
        }
        this.skip_button.width = this.skip_button._height / (this.skip_button.texture.height / this.skip_button.texture.width);
        this.skip_button.y = this.opt.baseHeight / 2;
        this.skip_button.interactive = true;
        this.skip_button.buttonMode = true;
        this.skip_button.defaultCursor = "pointer";
        this.skip_button.tap = this.skip_button.click = () => {
            /**
             * Google analytics code on skip
             */
            ga('send', 'event', 'Bike Shop', 'skip_clicked', this.opt.levels.group[this.currentLevel].name + '_Introduction');

            sound.stop();
            sound.play(this.opt.baseMediaUrl + this.opt.click_sound);
            // Remove all children
            this.play.removeChildren(0, this.play.children.length);
            // Remove all children from container
            this.container.removeChild(this.intro_bg);
            // Clear all time out
            for( let t = 0; t < this.timeouts.length; t += 1) {
                (this.timeouts[t]) ? window.clearTimeout(this.timeouts[t]) : '';
            }
            // Update the view
            this.updateView();
            window.removeEventListener('resize', this.resize);
            this.introResize = false;
            this.shopResize = true;
            window.addEventListener('resize', this.resizeGame.bind(this), false);
            this.flushTimeOuts();
            // Start the game
            this.startGame();
        }
        // Adding to container
        this.container.addChild(this.skip_button);
    }

	resize() {
	    if(this.introResize) {
            this.opt = window.app_setting;
            // Remove all children
            this.play.removeChildren(0, this.play.children.length);
            // Remove all children from container
            this.container.removeChild(this.intro_bg);
            // Clear all time out
            for( let t = 0; t < this.timeouts.length; t += 1) {
                (this.timeouts[t]) ? window.clearTimeout(this.timeouts[t]) : '';
            }
            // Update the view
            this.updateView();
            this.windowresized = true;
            this.playIntro();
            this.addBackground();
            this.addHomeIcon();
        }
	}

	resizeGame() {
	    if(this.shopResize) {
            this.opt = window.app_setting;
            sound.stop();
            // Remove all children
            this.game_stage.removeChildren(0, this.game_stage.children.length);
            this.game_stage.destroy();
            // Remove all children from container
            this.container.removeChild(this.game_stage);
            this.container.removeChild(this.intro_bg);
            this.container.removeChild(this.skip_button);

            this.play = new PIXI.Container();
            this.game_stage = new PIXI.Container();
            this.game = {};
            this.topCoins = new PIXI.Container();
            this.topCoinsGlow = new PIXI.Container();
            this.overLay = [];
            this.basketArr = [];
            this.selectedItem = {};
            this.animate = null;
            this.frameAnim = null;
            this.draggedItem = [];
            this.animArr = 0;
            //this.animationTimeout = null;
            this.selectedItemPrice = 0;
            // Clear all time out
            for( let t = 0; t < this.timeouts.length; t += 1) {
                (this.timeouts[t]) ? window.clearTimeout(this.timeouts[t]) : '';
            }
            this.windowresized = true;
            this.addHomeIcon();
            this.startGame();
            this.updateView();
        }
    }

    resizeBuildScreen() {
	    if(this.buildResize) {
            this.opt = window.app_setting;
	        this.game_stage.removeChildren(0, this.game_stage.children.length);
            console.log(this.delayTimeoutForDrag);
            if(this.delayTimeoutForDrag) {
                window.clearTimeout(this.delayTimeoutForDrag);
                this.delayTimeoutForDrag = null;
            }
            this.createProduct();
            this.updateView();
        }
    }

    resizePayoffScreen() {
        if(this.payoffResize) {
            this.opt = window.app_setting;
            this.game_stage.removeChildren(0, this.game_stage.children.length);
            sound.stop();
            if(this.animate) {
                this.animate.cancelAnimate();
                this.animate.destroy();
            }
            let animateOption = {
                imgArr : this.animArr,
                height: (this.opt.baseHeight / 1.5),
                ratio: 0.8,
                baseUrl: this.opt.baseImageUrl,
                x: (this.opt.baseWidth / 3.2),
                y: (this.opt.baseHeight / 3.4),
                renderer: this.renderer,
                container: this.container,
                containerHeight: this.opt.baseHeight,
                containerWidth: this.opt.baseWidth,
                background: this.opt.levels.group[this.currentLevel].climaxBg
            }
            this.animate = new Animate(animateOption);
            this.game_stage.addChild(this.animate.animate_container);
            this.animate.bgMoveAnimation();
            this.animate.climaxAnimate();

            sound.play(this.opt.baseMediaUrl + this.opt.levels.group[this.currentLevel].payoff, () => {
                //stop the sounds and release them
                sound.stop();
                //stop animation
                this.animate.cancelAnimate();
                //stop all frames
                this.stopFrame();
                //Destroy the container
                this.container.destroy();
                //update localStorage
                //localStorage.setItem('elmo', new Stringify(game_config).toString());
                window.app_setting = game_config;
                //get localStorage data and update the options
                this.opt = window.app_setting;

                window.removeEventListener('resize', this.resizePayoffScreen);
                this.payoffResize = false;
                //create new levels
                let levels = new Levels(new PIXI.Container(), this.opt, this.renderer);
                levels.addBackground();
                levels.addLevels();
                //Destroy all Objects
                this.animate.destroy();
                this.destroy();
                // Play next level audio
                levels.playLevelAudio();
            });

        }
    }

    flushTimeOuts() {
        if(this.touchtimeinterval) {
            window.clearTimeout(this.touchtimeinterval);
        }
	    this.timeouts.map( timeout => window.clearTimeout(timeout));
        this.timeouts = [];
    }
}
