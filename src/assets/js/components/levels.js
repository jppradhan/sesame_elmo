import Game from "../../../../src/assets/js/components/game.js";
import sound from "../../../../src/assets/js/components/sound.js";
import Loader from "../../../../src/assets/js/components/loader.js";
import sound from "../../../../src/assets/js/components/sound.js";
import Device from "../../../../src/assets/js/components/device.js";
import { Stringify } from "../../../../src/assets/js/components/utils.js";

/**
 * Levels class is to add levels to the game
 */
export default class Levels {
	//this.level = 1;
	/**
	 * [constructor assign the default values to variables]
	 * @param  {[Object]} stage [parent container]
	 * @param  {[Object]} o     [config object]
	 * @return {[Object]}       [context object]
	 */
	constructor(stage, o, renderer) {
		this.stage = stage;
		this.container = new PIXI.Container();
		this.opt = o;
		this.levelStage = '';
		this.levels = {};
		this.background = '';
		this.renderer = renderer;

		window.addEventListener('resize', ()=> {
		   this.resize();
        }, false);
		return this;
	}
	/**
	 * [addLevels add levels to the container]
	 */
	addLevels() {
		this.levelStage = new PIXI.Container();
		let gap = (this.opt.baseWidth / this.opt.levels.offset);
		let level_width = 0;
		//console.log(this.opt);
		for( let i = 0, l = this.opt.levels.group.length; i < l ; i += 1) {
			/**
			 * Adding first image
			 */
			let level = '';
			if(this.opt.levels.group[i].locked) {
				level = PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[i].image);
			} else {
				level = PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.group[i].image_active);
			}

			level.width = this.opt.baseWidth / 3.6571;
			level.height = level._width * (level.texture.height / level.texture.width);
			if(i == 0) {
				level_width = level._width;
			}
			level.x = (gap * (i + 1)) + (level_width * i);
			//console.log(gap, this.levels['level-' + i].width * i);
			level.y = (this.opt.baseHeight / 2) - 20;
			//console.log();
			level.interactive = true;
			level.buttonMode = true;
			level.defaultCursor = "pointer";
			//
			/*level.on('mousemove', () => {
				//console.log(this);
				Stringify.makeCursorPointer();
			});
			level.on('mouseout', () => {
				Stringify.makeCursorInitial();
			});*/

			level.tap = level.click = () => {

                if(this.opt.levels.group[i].locked) {
					return;
				}

				window.removeEventListener('resize', ()=> {
				   this.resize();
                });

                /**
                 * Add google analytics on click on character
                 */
                if(i == 0) {
                    ga('send', 'event', 'Bike Shop', 'game_start', 'Start');
                } else {
                    ga('send', 'event', 'Bike Shop', 'character_chosen', 'Start_'+ this.opt.levels.group[i].name);
                }

				// Clear the delay sound timeout
				window.clearTimeout(window.delaySound);
				//console.log('%c click', 'background-color: #0f0; color: #00f');
				sound.stop();
				sound.play(this.opt.baseMediaUrl + this.opt.click_sound);
				//clearInterval(playInterval);
				let loader = new Loader('loading');
				loader.show();
				let assetLoader = new PIXI.loaders.Loader();
                if(typeof cordova == 'undefined') {
                    for( let k = 0;k < this.opt.levels.group[i].audio_assets.length; k += 1) {
                        try{
                            assetLoader.add(this.opt.baseMediaUrl + this.opt.levels.group[i].audio_assets[k]);
                        } catch(e) {
                            console.log(e);
                        }
                        finally {
                            continue;
                        }

                    }
                }

				for( let k = 0;k < this.opt.levels.group[i].intro.user.length; k += 1) {
					//console.log(this.opt.levels.group[i].intro.user[k]);
					try{
						assetLoader.add(this.opt.baseImageUrl + this.opt.levels.group[i].intro.user[k]);
					} catch(e) {
						console.log(e);
					}
				}

				for( let m = 0; m < this.opt.levels.group[i].image_assets.length; m += 1) {
					try{
						assetLoader.add(this.opt.baseImageUrl + this.opt.levels.group[i].image_assets[m]);
					} catch(e) {
						console.log(e);
					}
					finally {
						continue;
					}

				}
				//assetLoader.add(CONFIG.baseMediaUrl + md);

				assetLoader.once('complete',(d) => {
					//console.log(d);
					setTimeout(() => {
						let game = new Game(window.app_setting, this.renderer, i);
						game.addHomeIcon();
						game.updateView();
						game.playIntro();
						loader.hide();
					}, 1000, i);

				});
				assetLoader.load();
			}
			//console.log(this.levels);
			/**
			 * add each levels to the container
			 */
			this.levels['level-' + i] = level;
			this.levelStage.addChild(level);
			//console.log(this.levels['level-' + i]);
		}
		/**
		 * Add all the levels to the parent stage
		 */
		this.updateStage();
		return this;
	}
	/**
	 * { function_description }
	 */
	updateStage() {
		this.container.addChild(this.levelStage);
		this.stage.addChild(this.container);
		this.updateView();
		return this;
	}
	/**
	 * [addBackground adding background to levels]
	 */
	addBackground() {
		this.background = PIXI.Sprite.fromImage(this.opt.baseImageUrl + this.opt.levels.bgImage);
		this.background.height = this.opt.baseHeight;
		this.background.width = this.opt.baseWidth;
		this.background.x = 0;
		this.background.y = 0;
		this.container.addChild(this.background);
		return this;
	}
	/**
	 * Play level audio after unlock
	 */
	playLevelAudio() {
		let count = 0;
		let d = new Device();
        if(d.isMobile()) {
            sound.play(this.opt.baseMediaUrl + this.opt.levels.levelAudio[0].mobile);
        } else {
            sound.play(this.opt.baseMediaUrl + this.opt.levels.levelAudio[0].desktop);
        }
	}

	updateView() {
		this.renderer.render(this.stage);
	}

	resize() {
        this.opt = window.app_setting;
        this.addLevels();
        this.addBackground();
        this.updateStage();
        this.updateView();
    }
}
