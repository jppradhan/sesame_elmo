/**
 * Loader bar
 */
export default class Loader {
	constructor(id) {
		this.id = id;
		this.interval = null;
		this.count = 0;
	}
	show() {
	    let progressbar = document.querySelector('.progress-bar');
	    let progress_count = document.querySelector('.progress-count');
		document.getElementById(this.id).style.display = 'block';
		progressbar.style.width = this.count + '%';
		progress_count.innerHTML =  this.count + '%';
		this.interval = setInterval(() => {
		    if(this.count < Math.floor((Math.random() + 9) * 10)) {
		         progressbar.style.width = this.count + '%';
                 progress_count.innerHTML =  this.count + '%';
                 this.count = this.count + 1;
		    }
		}, 200);
	}
	hide() {
	    this.count = 0;
	    window.clearInterval(this.interval);
	    let progressbar = document.querySelector('.progress-bar');
	    let progress_count = document.querySelector('.progress-count');
	    progressbar.style.width = '100%';
		progress_count.innerHTML =  '100%';
		setTimeout(() => {
		    document.getElementById(this.id).style.display = 'none';
		}, 300);
	}
}
