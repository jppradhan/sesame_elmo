/**
 * Single tone sound function
 */
class Sound {

	constructor() {
        this.callback = '';
        this.stoppedByUser = false;
        this.audioArr = [];
    }
	/**
	 * [play Play Audio]
	 * @param  {[String]}   path     [path of Audio File]
	 * @param  {Function} callback [callback after audio complete]
	 * @return {[Object]}            [null]
	 */
	play(path, callback) {
		/**
		 * [path path for the audio file]
		 * @type {[String]}
		 */
		this.path = path;
		/**
		 * [audio new instance for the Audio]
		 * @type {Object}
		 */
		// For Cordova
		if(typeof cordova != 'undefined') {
            //Cordova playing audio
            this.audio = new Media('file:///android_asset/www/' + path, (status) => {
                console.log(status);
            }, (err) => {
				console.log(err);
                if(err.code == 0) {
                    //console.log(this.audio);
                    this.audio.play();
                }
			}, (status) => {
                if(status == 4) {
                    if(this.stoppedByUser) {
                        this.stoppedByUser = false;
                    } else {
                        if(callback) {
                            callback();
                        }
                    }
                }
            });
            this.audio.play();
            this.audioArr.push(this.audio);
		} else {
			//console.log(PIXI.audioManager);
			// For nomal web browser and mobile browser
			// PIXI.audioManager
			this.audio = PIXI.audioManager.getAudio(path);
			// On audio end
			this.audio.on('end', () => {
				//console.log('ended');
				if(callback) {
					//console.log(callback);
					//this.isStopped = true;
					callback();
				}
			});
			// Play Audio

			this.audio.play();
		}

	}
	/**
	 * [pause Pause Audio]
	 * @return {[Object]} [Audio Object]
	 */
	pause() {
	    if(!this.audio) {
	       return;
        }
	    if(typeof cordova == 'undefined') {
            this.audio.stop();
        } else {
            this.audio.pause();
        }
    }
	resume() {
        if(!this.audio) {
            return;
        }
        this.audio.play();
	}
	/**
	 * [stop Stop the current Audio]
	 * @return {[Object]} [Object]
	 */
	stop() {
		// If Audio Object is defined
		if(this.audio) {
			if(typeof cordova == 'undefined') {
				try{
					//console.log(this.audio.audio);
					PIXI.audioManager.pause();
					// Remove audio instance from audio manager
					this.audio.remove();
					PIXI.audioManager.removeAudio(this.audio);
					//console.log(PIXI.audioManager);
					this.audio = null;
				} catch(e) {
					console.log(e);
				}

			} else {
                this.audio.stop();
                //this.audio.release();
                this.audioArr.map((aud) => {
                    aud.release();
                });
                this.audioArr = [];
                this.stoppedByUser = true;
            }
		}
	}
	/**
	 * [duration calculate the duration for Audio]
	 * @param  {Function} callback [callback after getting the Audio duration]
	 * @return {[Object]}            [Null]
	 */
	duration(path, callback) {
	    var audio, time;
	    if(typeof cordova == 'undefined') {
            audio = PIXI.audioManager.getAudio(path);
            time = audio.data.duration;
            if(callback) {
                callback(time);
            }
            PIXI.audioManager.removeAudio(audio);
        } else {
            audio = new Media('file:///android_asset/www/' + path, () => {
                if(callback) {
                    var dur = audio.getDuration();
                    if(dur > -1) {
                        audio.stop();
                        audio.release();
                        callback(dur)
                    }
                }
            }, (err) => {

            });
            audio.play();
            audio.setVolume(0);
        }
	}
}
/**
 * [sound Creating Instance for Audio]
 * @type {Sound}
 */
let sound = new Sound();
export default sound;
