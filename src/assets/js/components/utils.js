/**
 * { String manipulation Class }
 */
class Stringify {
	constructor(o) {
		this.str = o;
	}
	/**
	 * Returns a string representation of the object.
	 *
	 * @return     {<String>}  String representation of the object.
	 */
	static toString() {
		let cache = [];
		/**
		 * { Input String }
		 *
		 * @type       {<String>}
		 */
		let str = JSON.stringify(this.str, (key, value) => {
		    if (typeof value === 'object' && value !== null) {
		        if (cache.indexOf(value) !== -1) {
		            // Circular reference found, discard key
		            return;
		        }
		        // Store value in our collection
		        cache.push(value);
		    }
		    return value;
		});
		cache = null;
		return str;
	}

	static makeCursorPointer() {
		document.getElementsByTagName('body')[0].style.cursor = 'pointer';
	}

	static makeCursorInitial() {
		document.getElementsByTagName('body')[0].style.cursor = 'initial';
	}

	static makeFullScreen(id) {
        var i = document.getElementById(id);
        if (i.requestFullscreen) {
            i.requestFullscreen();
        } else if (i.webkitRequestFullscreen) {
            i.webkitRequestFullscreen();
        } else if (i.mozRequestFullScreen) {
            i.mozRequestFullScreen();
        } else if (i.msRequestFullscreen) {
            i.msRequestFullscreen();
        }
    }
}

export { Stringify };
